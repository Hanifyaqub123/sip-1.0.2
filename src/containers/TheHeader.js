import React,{useState} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CBreadcrumbRouter,
  CLink
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

// routes config
import routes from '../routes'

import { 
  TheHeaderDropdown,
}  from './index'
import { useHistory } from "react-router-dom";
import axios from 'axios'

const TheHeader = () => {

  const history = useHistory();
  const [namaLengkap,setNamaLengkap] = useState("")
  const [gelarDepan,setGelarDepan] = useState("")
  const [gelarBelakang,setGelarBelakang] = useState("")

  React.useEffect(()=>{
    var user_nik = localStorage.getItem("nik")
    
    if (user_nik === null) {
      history.push('/login')
    } else {
      axios.get("" + window.server + "rest-api-sip/profile/user/identitas/getprofile.php?nik=" + user_nik + "")
        .then((res)=>{
          var data = res.data[0]
          setNamaLengkap(data.nama_lengkap)
          setGelarDepan(data.gelar_akademis_depan)
          setGelarBelakang(data.gelar_akademis_belakang)
        })
        .catch((err)=>{
          console.log(err)
        })
    }
  },[])

  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.sidebarShow)

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch({type: 'set', sidebarShow: val})
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch({type: 'set', sidebarShow: val})
  }

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        <CIcon name="logo" height="48" alt="Logo"/>
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3" >
          <CHeaderNavLink to="/skp">Dashboard</CHeaderNavLink>
        </CHeaderNavItem>
      </CHeaderNav>

      <CHeaderNav className="px-3">
        <label style={{fontWeight:'bold'}}>{gelarDepan}&nbsp;{namaLengkap}&nbsp;{gelarBelakang}</label>
        <TheHeaderDropdown/>
      </CHeaderNav>

      <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter 
          className="border-0 c-subheader-nav m-0 px-0 px-md-3" 
          routes={routes} 
        />
         
      </CSubheader>
    </CHeader>
  )
}

export default TheHeader
