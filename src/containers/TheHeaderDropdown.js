import React,{useState} from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

import { useHistory } from "react-router-dom";
import axios from 'axios'

const TheHeaderDropdown = () => {

  const [image,setImage] = useState("")

  const history = useHistory();
  React.useEffect(()=>{
    var user_nik = localStorage.getItem("nik")
    
    if (user_nik === null) {
      history.push('/login')
    } else {
      axios.get("" + window.server + "rest-api-sip/profile/user/identitas/getprofile.php?nik=" + user_nik + "")
        .then((res)=>{
          var data = res.data[0]
          setImage(data.image)
        })
        .catch((err)=>{
          console.log(err)
        })
    }
  },[])
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={image}
            className="c-avatar-img"
            alt="profile bpn salatiga"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Settings</strong>
        </CDropdownItem>
        <CDropdownItem
          onClick={()=>{
            history.push('/identitasdiri')
          }}
        >
          <CIcon name="cil-user" className="mfe-2" />Profile
        </CDropdownItem>
        
        <CDropdownItem divider />
        <CDropdownItem
          onClick={()=>{
            history.push('/logout')
          }}
        >
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Keluar
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
