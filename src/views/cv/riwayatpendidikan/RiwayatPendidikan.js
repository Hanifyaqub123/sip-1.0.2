import React, { useState, useEffect } from "react";
import {
    CCol,
    CRow,
    CContainer,
    CFormGroup,
    CLabel,
    CInput,
    CCard,
    CButton,
    CButtonToolbar,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
  } from "@coreui/react";
  import axios from "axios";

  const RiwayatPendidikan = () =>{

    const [formEditData,setFormEditData] = useState(false);
    const [formAddData,setFormAddData] = useState(false);

    const [id,setId] = useState("");
    const [nik] = useState(localStorage.getItem('nik'))
    const [jenjang,setJenjang] = useState("");
    const [jurusan,setjurusan ] = useState("");
    const [sekolah,setSekolah] = useState("");
    const [nilai,setNilai] = useState("");
    const [tahunLulus,setTahunLulus] = useState("");
    const [noIjazah,setNoIjazah] = useState("");

    const [jenjangTambah,setJenjangTambah] = useState("");
    const [jurusanTambah,setjurusanTambah ] = useState("");
    const [sekolahTambah,setSekolahTambah] = useState("");
    const [nilaiTambah,setNilaiTambah] = useState("");
    const [tahunLulusTambah,setTahunLulusTambah] = useState("");
    const [noIjazahTambah,setNoIjazahTambah] = useState("");


    const [data,setData] = useState([])
    const [buttonTambah,setButtonSimpanAdd] = useState(true)


    useEffect(()=>{
      axios.get(""+window.server+"rest-api-sip/profile/user/riwayatpendidikan/getpendidikan.php?nik="+nik+"")
        .then((res)=>{
          if (res.data.message === "No post found") {
              setData([])
          } else {
            setData(res.data)
          }
        })
        .catch((err)=>{
          console.log(err)
        })
    },[])

    const handleJenjang = e =>{
      setJenjang(e.target.value)
    }
    const handleJurusan = e =>{
      setjurusan(e.target.value)
    }
    const handleSekolah = e =>{
      setSekolah(e.target.value)
    }
    const handleNilai = e =>{
      setNilai(e.target.value)
    }

    const handleTahunLulus = e =>{
      setTahunLulus(e.target.value)
    }

    const handleNoIjazah = e =>{
      setNoIjazah(e.target.value)
    }

    // -----------
    
    const handleJenjangTambah = e =>{
      setJenjangTambah(e.target.value)
    }
    const handleJurusanTambah = e =>{
      setjurusanTambah(e.target.value)
    }
    const handleSekolahTambah = e =>{
      setSekolahTambah(e.target.value)
    }
    const handleNilaiTambah = e =>{
      setNilaiTambah(e.target.value)
    }

    const handleTahunLulusTambah = e =>{
      setTahunLulusTambah(e.target.value)
    }

    const handleNoIjazahTambah = e =>{
      setNoIjazahTambah(e.target.value)
    }

    const handleUbahData = () =>{
      var post = {
        id: id,
        nik: nik,
        jenjang: jenjang,
        jurusan: jurusan,
        sekolah: sekolah,
        nilai: nilai,
        tahun_lulus: tahunLulus,
        no_ijazah: noIjazah  
      }
      axios.put(""+window.server+"rest-api-sip/profile/user/riwayatpendidikan/updatependidikan.php",post)
        .then((res)=>{
          setFormEditData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err)=>{
          console.log(err)
        })
    }

    const handleTambahData = () =>{
      setButtonSimpanAdd(false)
      var post = {
        id: id,
        nik: nik,
        jenjang: jenjangTambah,
        jurusan: jurusanTambah,
        sekolah: sekolahTambah,
        nilai: nilaiTambah,
        tahun_lulus: tahunLulusTambah,
        no_ijazah: noIjazahTambah  
      }

      if (jenjangTambah === "") {
        setButtonSimpanAdd(true)
        alert("Jenjang tidak boleh kosong")
      } else if (jurusanTambah === "") {
        setButtonSimpanAdd(true)
        alert("Jurusan tidak boleh kosong")
      } else if (sekolahTambah === "") {
        setButtonSimpanAdd(true)
        alert("Sekolah tidak boleh kosong")
      } else if (nilaiTambah === "") {
        setButtonSimpanAdd(true)
        alert("Nilai tidak boleh kosong")
      }else if (tahunLulusTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tahun Lulus tida boleh kosong")
      }else if (noIjazahTambah === "") {
        setButtonSimpanAdd(true)
        alert("Nomor Ijazah tidak boleh kosong")
      } else{
        axios.post(""+window.server+"rest-api-sip/profile/user/riwayatpendidikan/insertpendidikan.php",post)
        .then((res)=>{
          if (res.data.message === "Data Inserted") {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          } else {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          }
        })
        .catch((err)=>{
          setButtonSimpanAdd(true)
          console.log(err)
        })
      }
    }

    const handleHapusData = e => {
      if (window.confirm('Apakah anda yakin ?')) {
        axios
          .delete("" + window.server + "rest-api-sip/profile/user/riwayatpendidikan/deletependidikan.php", {
            data: { id: e.target.value },
          })
          .then((res) => {
            alert(JSON.stringify(res.data))
            window.location.reload();
          })
          .catch((err) => {
            console.log(err)
          })
      }
  
    }

      return(
    <CContainer>
             <CCard style={{padding:20}}>
        <CButtonToolbar justify="end">
        <CButton onClick={()=>{
          setFormAddData(true)
          setFormEditData(false)
        }} style={{marginRight:10}} color="success">Tambah</CButton>
       
      </CButtonToolbar>
      <CRow>
    <CCol  className="py-3">
<table>
  <tr>
    <th style={{width:'3%'}}>No</th>
    <th style={{width:'5%'}}>Jenjang </th>
    <th style={{width:'5%'}}>Jurusan</th>
    <th style={{width:'5%'}}>Sekolah/PT</th>
    <th style={{width:'5%'}}>Nilai/IPK</th>
    <th style={{width:'5%'}}>Tahun Lulus</th>
    <th style={{width:'5%'}}>No Ijazah</th>
    <th style={{width:'10%'}}>Aksi</th>
  </tr>
    {
      data.map((res,index)=>{
        return(
          <tr>
            <td>{index+1}</td>
            <td>{res.jenjang}</td>
            <td>{res.jurusan}</td>
            <td>{res.sekolah}</td>
            <td>{res.nilai}</td>
            <td>{res.tahun_lulus}</td>
            <td>{res.no_ijazah}</td>
            
          <td>
            <CButton onClick={()=>{
              setFormEditData(true)
              setFormAddData(false)
              setJenjang(res.jenjang)
              setjurusan(res.jurusan)
              setSekolah(res.sekolah)
              setNilai(res.nilai)
              setTahunLulus(res.tahun_lulus)
              setNoIjazah(res.no_ijazah)
              setId(res.id)
            }} style={{marginRight:10}} color="info">Ubah</CButton>
              <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>
          </td>
          </tr>
        )
      })
    }

 
</table>
        </CCol>
        
      </CRow>
  
      </CCard>

      {
        formEditData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Ubah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Jenjang
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleJenjang}
                      value={jenjang}
                      type="text"
                      placeholder="Jenjang"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Jurusan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={jurusan}
                      onChange={handleJurusan}
                      type="text"
                      placeholder="Jurusan"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Sekolah / PT
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={sekolah}
                      onChange={handleSekolah}
                      type="text"
                      placeholder="Sekolah / PT"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nilai / IPK
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={nilai}
                      onChange={handleNilai}
                      type="number"
                      placeholder="Nilai"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tahun Lulus
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tahunLulus}
                      onChange={handleTahunLulus}
                      type="number"
                      placeholder="Tahun Lulus"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nomor Ijazah
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={noIjazah}
                      onChange={handleNoIjazah}
                      type="text"
                      placeholder="Nomor Ijazah"
                    />
              </CInputGroup>
              <CButton onClick={handleUbahData} style={{marginRight:10}} color="success">Simpan</CButton>
          </CCol>
        </CRow>
        </CCard>
  
      }

      {
        formAddData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Tambah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Jenjang
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleJenjangTambah}
                      value={jenjangTambah}
                      type="text"
                      placeholder="Jenjang"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Jurusan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={jurusanTambah}
                      onChange={handleJurusanTambah}
                      type="text"
                      placeholder="Jurusan"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Sekolah / PT
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={sekolahTambah}
                      onChange={handleSekolahTambah}
                      type="text"
                      placeholder="Sekolah / PT"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nilai / IPK
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={nilaiTambah}
                      onChange={handleNilaiTambah}
                      type="number"
                      placeholder="Nilai"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tahun Lulus
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tahunLulusTambah}
                      onChange={handleTahunLulusTambah}
                      type="number"
                      placeholder="Tahun Lulus"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nomor Ijazah
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={noIjazahTambah}
                      onChange={handleNoIjazahTambah}
                      type="text"
                      placeholder="Nomor Ijazah"
                    />
              </CInputGroup>
              {
                buttonTambah === true &&
                <CButton onClick={handleTambahData} style={{marginRight:10}} color="success">Simpan</CButton>
              }
          </CCol>
        </CRow>
        </CCard>
  
      }
 

    </CContainer>
      )
  }

  export default RiwayatPendidikan