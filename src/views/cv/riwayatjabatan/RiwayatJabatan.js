import React, { useState, useEffect } from "react";
import {
  CCol,
  CRow,
  CContainer,
  CFormGroup,
  CLabel,
  CInput,
  CCard,
  CButton,
  CButtonToolbar,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CSpinner
} from "@coreui/react";
import axios from "axios";

const RiwayatJabatan = () => {

  const [formEditData, setFormEditData] = useState(false);
  const [formAddData, setFormAddData] = useState(false);
  // EDIT
  const [id, setId] = useState("")
  const [nik] = useState(localStorage.getItem("nik"))
  const [nomorSk, setNomorSK] = useState("");
  const [tanggalSk, setTangalSk] = useState("");
  const [namaJabatan, setNamaJabatan] = useState("");
  const [unitKerja, setUnitKerja] = useState("");
  // EDIT

  // TAMBAH
  const [nomorSkTambah, setNomorSKTambah] = useState("");
  const [tanggalSkTambah, setTangalSkTambah] = useState("");
  const [namaJabatanTambah, setNamaJabatanTambah] = useState("");
  const [unitKerjaTambah, setUnitKerjaTambah] = useState("");
  // TAMBAH

  const [data, setData] = useState([])

  const [buttonSimpanAdd, setButtonSimpanAdd] = useState(true)



  useEffect(() => {
    handleTableValue();
  }, [])

  const handleTableValue = () => {
    axios.get("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/getjabatan.php?nik=" + nik + "")
      .then((res) => {
        if (res.data.message === "No post found") {
          setData([])
        } else {
          setData(res.data)
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // EDIT
  const handleUbahNomorSk = e => {
    setNomorSK(e.target.value)
  }
  const handleUbahTanggal = e => {
    setTangalSk(e.target.value)
  }
  const handleUbahNamaJabatan = e => {
    setNamaJabatan(e.target.value)
  }
  const handleUbahUnitKerja = e => {
    setUnitKerja(e.target.value)
  }
  // EDIT

  // TAMBAH

  const handleTambahNomorSk = e => {
    setNomorSKTambah(e.target.value)
  }
  const handleTambahTanggal = e => {
    setTangalSkTambah(e.target.value)
  }
  const handleTambahNamaJabatan = e => {
    setNamaJabatanTambah(e.target.value)
  }
  const handleTambahUnitKerja = e => {
    setUnitKerjaTambah(e.target.value)
  }

  // TAMBAH

  const handleSimpanDataEdit = e => {
    // setLoading(true)
    var pos = {
      id: id,
      nik: nik,
      nomor_sk: nomorSk,
      tanggal_sk: tanggalSk,
      nama_jabatan: namaJabatan,
      unit_kerja: unitKerja

    }

    axios.put("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/updatejabatan.php", pos)
      .then((res) => {
        // setLoading(false)
        setFormEditData(false)
        alert(JSON.stringify(res.data))
        window.location.reload();
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const handleSimpanDataTambah = e => {
    setButtonSimpanAdd(false)
    var pos = {
      nik: nik,
      nomor_sk: nomorSkTambah,
      tanggal_sk: tanggalSkTambah,
      nama_jabatan: namaJabatanTambah,
      unit_kerja: unitKerjaTambah

    }

    if (nomorSkTambah === "") {
      setButtonSimpanAdd(true)
      alert("Nomor SK tidak boleh kosong")
    } else if (tanggalSkTambah === "") {
      setButtonSimpanAdd(true)
      alert("Tanggal SK tidak boleh kosong")
    } else if (namaJabatanTambah === "") {
      setButtonSimpanAdd(true)
      alert("Nama jabatan tidak boleh kosong")
    } else if (unitKerjaTambah === "") {
      setButtonSimpanAdd(true)
      alert("Unit kerja  tidak boleh kosong")
    } else {

      axios.post("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/insertjabatan.php", pos)
        .then((res) => {

          if (res.data.message === "Data Inserted") {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          } else {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          }

        })
        .catch((err) => {
          console.log(err)
        })

    }
  }

  const handleHapusData = e => {
    if (window.confirm('Apakah anda yakin ?')) {
      axios
        .delete("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/deletejabatan.php", {
          data: { id: e.target.value },
        })
        .then((res) => {
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err) => {
          console.log(err)
        })
    }

  }

  return (
    <CContainer>
      <CCard style={{ padding: 20 }}>
        <CButtonToolbar justify="end">
          <CButton onClick={() => {
            setFormAddData(true)
            setFormEditData(false)
          }} style={{ marginRight: 10 }} color="success">Tambah</CButton>

        </CButtonToolbar>
        <CRow>
          <CCol className="py-3">
            <table>
              <tr>
                <th style={{ width: '3%' }}>No</th>
                <th style={{ width: '20%' }}>Nomor SK</th>
                <th style={{ width: '20%' }}>Tanggal SK</th>
                <th>Nama Jabatan</th>
                <th style={{ width: '20%' }}>Unit Kerja</th>
                <th style={{ width: '20%' }}>Aksi</th>
              </tr>
              {
                data === [] &&
                <></>
              }
              {
                data.map((res, index) => {
                  return (
                    <tr>
                      <td>{index + 1}</td>
                      <td>{res.nomor_sk}</td>
                      <td>{res.tanggal_sk}</td>
                      <td>{res.nama_jabatan}</td>
                      <td>{res.unit_kerja}</td>
                      <td>
                        <CButton onClick={() => {
                          setFormEditData(true)
                          setFormAddData(false)
                          setId(res.id)
                          setNomorSK(res.nomor_sk)
                          setTangalSk(res.tanggal_sk)
                          setNamaJabatan(res.nama_jabatan)
                          setUnitKerja(res.unit_kerja)
                        }} style={{ marginRight: 10 }} color="info">Ubah</CButton>
                        <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>
                        {/* { loading === false &&
                    <div style={{position:'absolute',top:'50%',left:'45%',}}>
                    <CSpinner
                      color="primary"
                      style={{width:'3rem', height:'3rem'}}
                    />
              
              </div>
      } */}
                      </td>
                    </tr>
                  )
                })
              }


            </table>
          </CCol>

        </CRow>

      </CCard>

      {
        formEditData === true &&
        <CCard>
          <CRow style={{ padding: 20 }}>
            <CCol lg="12" className="py-3">
              <h3>Ubah Data</h3>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nomor SK
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  onChange={handleUbahNomorSk}
                  value={nomorSk}
                  type="text"
                  placeholder="Nomor SK"
                  autoComplete="nomor-sk"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Tanggal SK
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={tanggalSk}
                  onChange={handleUbahTanggal}
                  type="date"
                  autoComplete="tanggal-sk"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nama Jabatan
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={namaJabatan}
                  onChange={handleUbahNamaJabatan}
                  type="text"
                  placeholder="Nama Jabatan"
                  autoComplete="nama-jabatan"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Unit Kerja
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={unitKerja}
                  onChange={handleUbahUnitKerja}
                  type="text"
                  placeholder="Unit Kerja"
                  autoComplete="unit-kerja"
                />
              </CInputGroup>
              <CButton onClick={() => {
                handleSimpanDataEdit()
              }} style={{ marginRight: 10 }} color="success">Simpan
              </CButton>
              {/* { loading === false &&
                    <div style={{position:'absolute',bottom:0,left:'50%',}}>
                    <CSpinner
                      color="primary"
                      style={{width:'3rem', height:'3rem'}}
                    />
              
              </div>
      } */}
            </CCol>
          </CRow>
        </CCard>

      }

      {
        formAddData === true &&
        <CCard>
          <CRow style={{ padding: 20 }}>
            <CCol lg="12" className="py-3">
              <h3>Tambah Data</h3>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nomor SK
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={nomorSkTambah}
                  onChange={handleTambahNomorSk}
                  type="text"
                  placeholder="Nomor SK"
                  autoComplete="nomor-sk"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Tanggal SK
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={tanggalSkTambah}
                  onChange={handleTambahTanggal}
                  type="date"
                  autoComplete="tanggal-sk"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nama Jabatan
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={namaJabatanTambah}
                  onChange={handleTambahNamaJabatan}
                  type="text"
                  placeholder="Nama Jabatan"
                  autoComplete="nama-jabatan"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Unit Kerja
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={unitKerjaTambah}
                  onChange={handleTambahUnitKerja}
                  type="text"
                  placeholder="Unit Kerja"
                  autoComplete="unit-kerja"
                />
              </CInputGroup>

              {
                buttonSimpanAdd === true &&
                <CButton onClick={() => {
                  handleSimpanDataTambah();
                }} style={{ marginRight: 10 }} color="success">Simpan
                </CButton>
              }



            </CCol>

          </CRow>
        </CCard>
      }
    </CContainer>
  )
}

export default RiwayatJabatan