import React, { useState, useEffect } from "react";
import {
  CCol,
  CRow,
  CSelect,
  CContainer,
  CFormGroup,
  CLabel,
  CInput,
  CCard,
  CButton,
  CButtonToolbar,
  CSpinner
} from "@coreui/react";
import axios from "axios";
import profile from '../../../assets/profile.png'
import ImageUploading from "react-images-uploading";

const IdentitasDiri = () => {
  const [namaLengkap, setNamaLengkap] = useState("")
  const [nomorHP, setNomorHP] = useState("")
  const [jenisKelamin,setJenisKelamin] = useState("")
  const [nik, setNik] = useState("");
  const [id, setId] = useState("")
  const [agama, setAgama] = useState("");
  const [gelarNonAkademisD, setGelarNonAkademisD] = useState("");
  const [gelarNonAkademisB, setGelarNonAkademisB] = useState("");
  const [gelarAkademisD, setGelarAkademisD] = useState("");
  const [gelarAkademisB, setGelarAkademisB] = useState("");
  const [statusPernikahan, setStatusPernikahan] = useState("");
  const [golonganDarah, setGolonganDarah] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tanggallahir, setTanggallahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [rt, setRt] = useState("");
  const [rw, setRw] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [imagePP, setImagePP] = useState("");
  const [pendidikanTerkahir, setPendidiaknTerakhir] = useState("");
  const [formasiJabatan, setFormasiJabatan] = useState("");
  const [nip, setNip] = useState("");

  const [provinsi, setProvinsi] = useState([]);
  const [provinsiValue, setProvinsiValue] = useState("");

  const [kota, setKota] = useState([]);
  const [kotaValue, setKotaValue] = useState("");

  const [kecamatan, setkecamatan] = useState([]);
  const [kecamatanValue, setkecamatanValue] = useState("");

  const [kodePos, setKodePos] = useState("")
  const [loading, setLoading] = useState(false)
  const [loadingImage, setLoadingImage] = useState(false)


  useEffect(() => {
    handleProvisni();
    handleValueIdentitas();
  }, []);

  const handleValueIdentitas = () => {
    var nik = localStorage.getItem("nik")
    axios.get("" + window.server + "rest-api-sip/profile/user/identitas/getprofile.php?nik=" + nik + "")
      .then((res) => {
        // alert(JSON.stringify(res.data[0]))
        var data = res.data[0]
        setNamaLengkap(data.nama_lengkap)
        setNomorHP(data.no_telp)
        setJenisKelamin(data.jenis_kelamin)
        setGelarAkademisD(data.gelar_akademis_depan)
        setGelarAkademisB(data.gelar_akademis_belakang)
        setGelarNonAkademisD(data.gelar_non_akademis_depan)
        setGelarNonAkademisB(data.gelar_non_akademis_belakang)
        setStatusPernikahan(data.status_pernikahan)
        setTempatLahir(data.tempat_lahir)
        setAlamat(data.alamat)
        setRt(data.rt)
        setRw(data.rw)
        setProvinsiValue(data.provinsi)
        setKotaValue(data.kabupaten)
        setkecamatanValue(data.kecamatan)
        setKodePos(data.kode_pos)
        setAgama(data.agama)
        setTanggallahir(data.tanggal_lahir)
        setGolonganDarah(data.golongan_darah)
        setFormasiJabatan(data.formasi_jabatan)
        setEmail(data.email)
        setPassword(data.password)
        setNip(data.nip)
        setPendidiaknTerakhir(data.pendidikan_terakhir)
        setImagePP(data.image)
        setNik(data.nik)
        setId(data.id)

        localStorage.setItem("nik",data.nik)
        localStorage.setItem("id",data.id)
        localStorage.setItem("nama_lengkap",data.nama_lengkap)
        localStorage.setItem("image",data.image)
        localStorage.setItem("no_telp",data.no_telp)
        
      })
      .catch((err) => {
        console.log(err)
      })
  }

  // const imageHandler = (e) => {
  //   const reader = new FileReader();
  //   reader.onload = () => {
  //     // console.log(reader.result);
  //     // localStorage.setItem('recent-image',reader.result);

  //     if (reader.readyState === 2) {
  //       setImagePP(reader.result);
  //     }
  //   };
  //   if (e.target.files[0]) {
  //     reader.readAsDataURL(e.target.files[0]);
  //   }

  //   console.log(imagePP);
  // };

  const imageHandler = (imageList) => {
    setLoadingImage(true)
    console.log(imageList[0].file.size / 1024 / 1024 + "MiB")
    var value = imageList[0].file.size / 1024 / 1024
    if (value < 1.26) {
      setLoadingImage(false)
      setImagePP(imageList[0].dataURL)
      alert("Upload berhasil")
    } else {
      setLoadingImage(false)
      alert("Upss.... ukuran max 1 MB")
    }
  };
  function handleProvisni() {
    axios
      .get("https://dev.farizdotid.com/api/daerahindonesia/provinsi")
      .then((res) => {
        setProvinsi(res.data.provinsi);
      });
  }

  const handleProvinsiId = (e) => {
    var result = e.target.value

    var nama = result.split(",").pop()
    var id = result.substring(0, 2)

    axios
      .get(
        "https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=" +
        id +
        ""
      )
      .then((res) => {
        setKota(res.data.kota_kabupaten);
        setProvinsiValue(nama)
      });
  };

  const handleKotaId = (e) => {
    var result = e.target.value

    var nama = result.split(",").pop()
    var id = result.substring(0, 4)

    axios
      .get(
        "https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=" +
        id +
        ""
      )
      .then((res) => {
        setkecamatan(res.data.kecamatan);
        setKotaValue(nama)
      });
  };

  const handleKecamatan = e => {
    setkecamatanValue(e.target.value)
  }

  const handleNamaLengkap = e => {
    setNamaLengkap(e.target.value)
  }

  const handleNik = e => {
    setNik(e.target.value)
  }

  const handleAgama = e => {
    setAgama(e.target.value)
  }

  const handleGelarNonAkademisDepan = e => {
    setGelarNonAkademisD(e.target.value)
  }

  const handleGelarNonAkademisBelakang = e => {
    setGelarNonAkademisB(e.target.value)
  }

  const handleGelarAkademisDepan = e => {
    setGelarAkademisD(e.target.value)
  }

  const handleGelarAkademisBelakang = e => {
    setGelarAkademisB(e.target.value)
  }

  const handleStatusPernikahan = e => {
    setStatusPernikahan(e.target.value)
  }

  const handleGolonganDarah = e => {
    setGolonganDarah(e.target.value)
  }

  const handleTempatLahir = e => {
    setTempatLahir(e.target.value)
  }

  const handleTanggalLahir = e => {
    setTanggallahir(e.target.value)
  }

  const handleAlamat = e => {
    setAlamat(e.target.value)
  }

  const handleRT = e => {
    setRt(e.target.value)
  }

  const handleRW = e => {
    setRw(e.target.value)
  }

  const handleKodePos = e => {
    setKodePos(e.target.value)
  }

  const handlePendidikanTerakhir = e => {
    setPendidiaknTerakhir(e.target.value)
  }

  const handleFormasiJabatan = e => {
    setFormasiJabatan(e.target.value)
  }

  const handleNIP = e => {
    setNip(e.target.value)
  }

  const handleEmail = e => {
    setEmail(e.target.value)
  }

  const handlePassword = e => {
    setPassword(e.target.value)
  }

  const handleJenisKelamin = e =>{
    setJenisKelamin(e.target.value)
  }

  const handleNomorHp = e =>{
    setNomorHP(e.target.value)
  }

  const handleSimpan = () => {
    setLoading(true)

    const post = {
      id: id,
      nik: nik,
      nama_lengkap: namaLengkap,
      no_telp: nomorHP,
      jenis_kelamin : jenisKelamin,
      gelar_non_akademis_depan: gelarNonAkademisD,
      gelar_akademis_depan: gelarAkademisD,
      status_pernikahan: statusPernikahan,
      alamat: alamat,
      tempat_lahir: tempatLahir,
      rt: rt,
      rw: rw,
      provinsi: provinsiValue,
      kabupaten: kotaValue,
      kecamatan: kecamatanValue,
      kode_pos: kodePos,
      pendidikan_terakhir: pendidikanTerkahir,
      gelar_non_akademis_belakang: gelarNonAkademisB,
      gelar_akademis_belakang: gelarAkademisB,
      agama: agama,
      tanggal_lahir: tanggallahir,
      golongan_darah: golonganDarah,
      formasi_jabatan: formasiJabatan,
      nip: nip,
      email: email,
      password: password,
      image: imagePP
    }

    axios.post("" + window.server + "rest-api-sip/profile/user/identitas/updateprofile.php", post)
      .then((res) => {
        setLoading(false)
        alert(JSON.stringify(res.data))
        window.location.reload();
      })
      .catch((err) => {
        setLoading(false)
        console.log(err)
      })

  }


  return (
    <div>
      <CCard style={{ padding: 20 }}>
        <div style={{ textAlign: "center", marginBottom: "2%" }}>
          <div>
            <img
              alt="Foto Profile"
              src={imagePP}
              style={{
                width: 150,
                height: 150,
                borderRadius: "50%",
                marginLeft: "30%",
                marginRight: "30%",
                marginBottom: "2%",
              }}
            />
          </div>
          <div>

            <label style={{ fontWeight: "bold" }}>{gelarAkademisD}&nbsp;{namaLengkap}&nbsp;{gelarAkademisB}</label>
          </div>
          <div>
            <label>PPNPN</label>
          </div>
        </div>
        <CContainer>
          <CRow>
            <CCol md="6" className="py-3">
              <CFormGroup>
                <CLabel>NIK</CLabel>
                <CInput
                  disabled
                  onChange={handleNik}
                  type="text"
                  id="nik"
                  name="nik"
                  value={nik}
                  placeholder="NIK"
                  autoComplete="nik"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Jenis Kelamin</CLabel>
                <CSelect onChange={handleJenisKelamin}>
                  <option value={jenisKelamin}>{jenisKelamin}</option>
                  <option value="Laki-Laki">Laki-Laki</option>\
                  <option value="Perempuan">Perempuan</option>
                </CSelect>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Gelar Non Akademis Depan</CLabel>
                <CInput
                  onChange={handleGelarNonAkademisDepan}
                  type="text"
                  value={gelarNonAkademisD}
                  id="gelar-non-akademis-depan"
                  name="gelar-non-akademis--depan"
                  placeholder="Gelar Non Akademis Depan"
                  autoComplete="gelar-non-akademis-depan"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Gelar Akademis Depan</CLabel>
                <CInput
                  onChange={handleGelarAkademisDepan}
                  type="text"
                  value={gelarAkademisD}
                  id="gelar-akademis-depan"
                  name="gelar-akademis-depan"
                  placeholder="Gelar Akademis Depan"
                  autoComplete="gelar-akademis-depan"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Status Pernikahan</CLabel>
                {
                  statusPernikahan === "Belum Menikah" &&
                  <select onChange={handleStatusPernikahan} class="form-control">
                    <option value={statusPernikahan}>{statusPernikahan}</option>
                    <option value="Sudah Menikah">Sudah Menikah</option>
                  </select>
                }
                {
                  statusPernikahan === "Sudah Menikah" &&
                  <select onChange={handleStatusPernikahan} class="form-control">
                    <option value={statusPernikahan}>{statusPernikahan}</option>
                    <option value="Belum Menikah">Belum Menikah</option>
                  </select>
                }
              </CFormGroup>
              <CFormGroup>
                <CLabel>Tempat Lahir</CLabel>
                <CInput
                  onChange={handleTempatLahir}
                  value={tempatLahir}
                  type="text"
                  id="tempat-lahir"
                  name="tempat-lahir"
                  placeholder="Tempat Lahir"
                  autoComplete="tempat-lahir"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Alamat</CLabel>
                <CInput
                  onChange={handleAlamat}
                  value={alamat}
                  type="text"
                  id="alamat"
                  name="alamat"
                  placeholder="Alamat"
                  autoComplete="alamat"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>RT</CLabel>
                <CInput onChange={handleRT} value={rt} type="number" placeholder="RT" autoComplete="rt" />
              </CFormGroup>

              <CFormGroup>
                <CLabel>RW</CLabel>
                <CInput onChange={handleRW} value={rw} type="number" placeholder="RW" autoComplete="rw" />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Provinsi</CLabel>
                <select
                  onChange={handleProvinsiId}
                  class="form-control"
                >
                  <option value={provinsiValue}>{provinsiValue}</option>
                  {provinsi.map((res, index) => {
                    return (
                      <option key={index} value={[res.id, res.nama]}>
                        {res.nama}
                      </option>
                    );
                  })}
                </select>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Kabupaten</CLabel>
                <select
                  onChange={handleKotaId}
                  class="form-control"
                >
                  <option value={kotaValue}>{kotaValue}</option>
                  {kota.map((res, index) => {
                    return (
                      <option key={index} value={[res.id, res.nama]}>
                        {res.nama}
                      </option>
                    );
                  })}
                </select>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Kecamatan</CLabel>
                <select
                  onChange={handleKecamatan}
                  class="form-control"                      >
                  <option value={kecamatanValue}>{kecamatanValue}</option>
                  {kecamatan.map((res, index) => {
                    return (
                      <option key={index} value={res.nama}>
                        {res.nama}
                      </option>
                    );
                  })}
                </select>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Kode Pos</CLabel>
                <CInput
                  onChange={handleKodePos}
                  value={kodePos}
                  type="number"
                  placeholder="Kode Pos"
                  autoComplete="kode-pos"
                />
              </CFormGroup>
              
            </CCol>
            <CCol sm="6" className="py-3">
              <CFormGroup>
                <CLabel>Nama Lengkap</CLabel>
                <CInput
                  onChange={handleNamaLengkap}
                  value={namaLengkap}
                  type="text"
                  placeholder="Nama Lengkap"
                  autoComplete="nama-lengkap"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Gelar Non Akademis Belakang</CLabel>
                <CInput
                  onChange={handleGelarNonAkademisBelakang}
                  value={gelarNonAkademisB}
                  type="text"
                  id="gelar-non-akademis-belakang"
                  name="gelar-non-akademis-belakang"
                  placeholder="Gelar Non Akademis Belakang"
                  autoComplete="gelar-non-akademis-belakang"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Gelar Akademis Belakang</CLabel>
                <CInput
                  onChange={handleGelarAkademisBelakang}
                  value={gelarAkademisB}
                  type="text"
                  id="gelar-akademis-belakang"
                  name="gelar-akademis-belakang"
                  placeholder="Gelar Akademis Belakang"
                  autoComplete="gelar-akademis-belakang"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Agama</CLabel>
                <select onChange={handleAgama} class="form-control">
                  <option value={agama}>{agama}</option>
                  <option value="Islam">Islam</option>
                  <option value="Kristen Protestan">Kristen Protestan</option>
                  <option value="Kristen Katolik">Kristen Katolik</option>
                  <option value="Hindu">Hindu</option>
                  <option value="Buddha">Buddha</option>
                  <option value="Konghucu">Konghucu</option>
                </select>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Tanggal Lahir</CLabel>
                <CInput
                  onChange={handleTanggalLahir}
                  value={tanggallahir}
                  type="date"
                  placeholder="Tanggal Lahir"
                  autoComplete="tanggal-lahir"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Golongan Darah</CLabel>
                <select onChange={handleGolonganDarah} class="form-control">
                  <option value={golonganDarah}>{golonganDarah}</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="AB">AB</option>
                  <option value="O">O</option>
                </select>
              </CFormGroup>
              <CFormGroup>
                <CLabel>Formasi Jabatan</CLabel>
                <CInput
                  onChange={handleFormasiJabatan}
                  value={formasiJabatan}
                  type="text"
                  placeholder="Formasi Jabatan"
                  autoComplete="formasi-jabatan"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>NIP</CLabel>
                <CInput onChange={handleNIP} value={nip} type="number" placeholder="NIP" autoComplete="nip" />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Email</CLabel>
                <CInput onChange={handleEmail} value={email} type="email" placeholder="email" autoComplete="email" />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Password</CLabel>
                <CInput
                  onChange={handlePassword}
                  value={password}
                  type="text"
                  placeholder="Password"
                  autoComplete="password"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Nomor HP</CLabel>
                <CInput
                  onChange={handleNomorHp}
                  value={nomorHP}
                  type="number"
                  placeholder="Nomor HP"
                  autoComplete="nomor-hp"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Pendidikan Terakhir</CLabel>
                <CInput
                  onChange={handlePendidikanTerakhir}
                  value={pendidikanTerkahir}
                  type="text"
                  placeholder="Pendidikan Terakhir"
                  autoComplete="pendidikan-terakhir"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel style={{ fontWeight: "bold" }}>GANTI FOTO</CLabel>
                <ImageUploading
                  onChange={imageHandler}
                >
                  {({ imageList, onImageUpload }) => (
                    // write your building UI
                    <div className="imageuploader">

                      <div className="mainBtns">
                        <button className="btn btn-primary mr-1" onClick={onImageUpload}>Ganti Foto</button>

                      </div>
                      {imageList.map((image) => (
                        <div className="imagecontainer" key={image.key}>
                          <img src={image.dataURL} />
                        </div>
                      ))}
                    </div>
                  )}
                </ImageUploading>
                {
                  loadingImage === true &&
                  <CSpinner style={{ marginTop: 15 }} color="info" />
                }

                {/* <CInput
                  style={{
                    height: "100%",
                    background: "transparent",
                    border: "none",
                  }}
                  onChange={imageHandler}
                  type="file"
                  id="input"
                  name="image-upload"
                  accept="image/*"
                /> */}
              </CFormGroup>
              <CButtonToolbar justify="end">
                <CButton onClick={handleSimpan} color="success">Simpan</CButton>
              </CButtonToolbar>
              {
                loading === true &&
                <CSpinner style={{ marginLeft: '50%' }} color="info" />
              }
            </CCol>
          </CRow>
        </CContainer>
      </CCard>
    </div>
  );
};

export default IdentitasDiri;
