import React, { useState, useEffect } from "react";
import {
    CCol,
    CRow,
    CContainer,
    CFormGroup,
    CLabel,
    CInput,
    CCard,
    CButton,
    CButtonToolbar,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
  } from "@coreui/react";
  import axios from "axios";

  const RiwayatKinerja = () =>{

    const [formEditData,setFormEditData] = useState(false);
    const [formAddData,setFormAddData] = useState(false);

    const [id,setId] = useState("")
    const [nik] = useState(localStorage.getItem('nik'))
    const [tahunPenilaian,setTahunPenilaian] = useState("");
    const [tanggalPenilaian,setTanggalPenilaian] = useState("");
    const [namaJabatan,setNamaJabatan] = useState("");
    const [unitKerja,setUnitKerja] = useState("");
    const [skorPenilaian,setSkorPenilaian] = useState("");

    const [tahunPenilaianTambah,setTahunPenilaianTambah] = useState("");
    const [tanggalPenilaianTambah,setTanggalPenilaianTambah] = useState("");
    const [namaJabatanTambah,setNamaJabatanTambah] = useState("");
    const [unitKerjaTambah,setUnitKerjaTambah] = useState("Kantah Kota Salatiga");
    const [skorPenilaianTambah,setSkorPenilaianTambah] = useState("");

    const [data,setData] = useState([]);
    const [buttonSimpanTambah,setButtonSimpanTambah] = useState(false)


    useEffect(()=>{
      axios.get(""+window.server+"rest-api-sip/profile/user/riwayatkinerja/getkinerja.php?nik="+nik+"")
        .then((res)=>{
          if (res.data.message === "No post found") {
            setData([])
          } else {
            setData(res.data)
          }
        })
        .catch((err)=>{
          console.log(err)
        })
    },[])

    const handleTahunPenilaian = e =>{
      setTahunPenilaian(e.target.value)
    }

    const handleTanggalPenilaian = e =>{
      setTanggalPenilaian(e.target.value)
    }

    const handleNamaJabatan = e =>{
      setNamaJabatan(e.target.value)
    }

    const handleUnitKerja = e =>{
      setUnitKerja(e.target.value)
    }

    const handleSkorPenilaian = e =>{
      setSkorPenilaian(e.target.value)
    }

// ---------------------------------------------
    const handleTahunPenilaianTambah = e =>{
      setTahunPenilaianTambah(e.target.value)
    }

    const handleTanggalPenilaianTambah = e =>{
      setTanggalPenilaianTambah(e.target.value)
    }

    const handleNamaJabatanTambah = e =>{
      setNamaJabatanTambah(e.target.value)
    }

    const handleUnitKerjaTambah = e =>{
      setUnitKerjaTambah(e.target.value)
    }

    const handleSkorPenilaianTambah = e =>{
      setSkorPenilaianTambah(e.target.value)
    }

    const handleUbahData = () =>{
      var post = {
        id : id,
        nik : nik,
        tahun_penilaian : tahunPenilaian,
        tanggal_penilaian : tanggalPenilaian,
        nama_jabatan : namaJabatan,
        unit_kerja : unitKerja,
        skor_penilaian : skorPenilaian
      }

      axios.put(""+window.server+"rest-api-sip/profile/user/riwayatkinerja/updatekinerja.php",post)
        .then((res)=>{
          setFormEditData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err)=>{
          console.log(err)
        })
    }

    const handleTambahData = () =>{
     setButtonSimpanTambah(true)
      var post = {
        nik : nik,
        tahun_penilaian : tahunPenilaianTambah,
        tanggal_penilaian : tanggalPenilaianTambah,
        nama_jabatan : namaJabatanTambah,
        unit_kerja : unitKerjaTambah,
        skor_penilaian : skorPenilaianTambah
      }

      if (tahunPenilaianTambah === "") {
        setButtonSimpanTambah(false)
        alert("Tahun penilaian tidak boleh")
      } else if (tanggalPenilaianTambah === "") {
        setButtonSimpanTambah(false)
        alert("Tanggal penilaian tidak boleh")
      } else if (namaJabatanTambah === "") {
        setButtonSimpanTambah(false)
        alert("Nama jabatan tidak boleh")
      } else if (unitKerjaTambah === "") {
        setButtonSimpanTambah(false)
        alert("Unit kerja tidak boleh")
      } else if (skorPenilaianTambah === "") {
        setButtonSimpanTambah(false)
        alert("Skor penilaian tidak boleh")
      } else{
        axios.post(""+window.server+"rest-api-sip/profile/user/riwayatkinerja/insertkinerja.php",post)
        .then((res)=>{
          setButtonSimpanTambah(false)
          setFormAddData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .then((err)=>{
          setButtonSimpanTambah(false)
          console.log(err)
        })
      }
    }

    const handleHapusData = e => {
      if (window.confirm('Apakah anda yakin ?')) {
        axios
          .delete("" + window.server + "rest-api-sip/profile/user/riwayatkinerja/deletekinerja.php", {
            data: { id: e.target.value },
          })
          .then((res) => {
            alert(JSON.stringify(res.data))
            window.location.reload();
          })
          .catch((err) => {
            console.log(err)
          })
      }
  
    }



    

      return(
    <CContainer>
             <CCard style={{padding:20}}>
        <CButtonToolbar justify="end">
        <CButton onClick={()=>{
          setFormAddData(true)
          setFormEditData(false)
        }} style={{marginRight:10}} color="success">Tambah</CButton>
       
      </CButtonToolbar>
      <CRow>
    <CCol  className="py-3">
<table>
  <tr>
    <th style={{width:'3%'}}>No</th>
    <th style={{width:'10%'}}>Tahun Penilaian</th>
    <th style={{width:'10%'}}>Tanggal Penilaian</th>
    <th style={{width:'10%'}}>Nama Jabatan</th>
    <th style={{width:'10%'}}>Unit Kerja</th>
    <th style={{width:'10%'}}>Skor Penilaian</th>
    <th style={{width:'15%'}}>Aksi</th>
  </tr>
    {
      data.map((res,index)=>{
        return(
          <tr>
            <td>{index+1}</td>
            <td>{res.tahun_penilaian}</td>
            <td>{res.tanggal_penilaian}</td>
            <td>{res.nama_jabatan}</td>
            <td>{res.unit_kerja}</td>
            <td>{res.skor_penilaian}</td>
          <td>
            <CButton onClick={()=>{
              setFormEditData(true)
              setFormAddData(false)
              setTahunPenilaian(res.tahun_penilaian)
              setTanggalPenilaian(res.tanggal_penilaian)
              setNamaJabatan(res.nama_jabatan)
              setUnitKerja(res.unit_kerja)
              setSkorPenilaian(res.skor_penilaian)
              setId(res.id)
            }} style={{marginRight:10}} color="info">Ubah</CButton>
              <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>
          </td>
          </tr>
        )
      })
    }

 
</table>
        </CCol>
        
      </CRow>
  
      </CCard>

      {
        formEditData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Ubah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tahun Penilaian
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleTahunPenilaian}
                      value={tahunPenilaian}
                      type="number"
                      placeholder="Tahun Penilaian"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Penilaian
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalPenilaian}
                      onChange={handleTanggalPenilaian}
                      type="date"
                      autoComplete="tanggal-penilaian"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Jabatan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={namaJabatan}
                      onChange={handleNamaJabatan}
                      type="text"
                      placeholder="Nama Jabatan"
                      autoComplete="nama-jabatan"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Unit Kerja
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      disabled
                      value={unitKerja}
                      onChange={handleUnitKerja}
                      type="text"
                      placeholder="Unit Kerja"
                      autoComplete="unit-kerja"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Skor Penilaian
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={skorPenilaian}
                      onChange={handleSkorPenilaian}
                      type="text"
                      placeholder="Skor Penilaian"
                      autoComplete="skor-penilaian"
                    />
              </CInputGroup>
              <CButton onClick={handleUbahData} style={{marginRight:10}} color="success">Simpan</CButton>
          </CCol>
        </CRow>
        </CCard>
  
      }

      {
        formAddData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Tambah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tahun Penilaian
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tahunPenilaianTambah}
                      onChange={handleTahunPenilaianTambah}
                      type="number"
                      placeholder="Tahun Penilaian"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Penilaian
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalPenilaianTambah}
                      onChange={handleTanggalPenilaianTambah}
                      type="date"
                      autoComplete="tanggal-sk"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Jabatan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={namaJabatanTambah}
                      onChange={handleNamaJabatanTambah}
                      type="text"
                      placeholder="Nama Jabatan"
                      autoComplete="nama-jabatan"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Unit Kerja
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      disabled
                      value={unitKerjaTambah}
                      onChange={handleUnitKerjaTambah}
                      type="text"
                      placeholder="Unit Kerja"
                      autoComplete="unit-kerja"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Skor Penilaian
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={skorPenilaianTambah}
                      onChange={handleSkorPenilaianTambah}
                      type="number"
                      placeholder="Skor Penilaian"
                      autoComplete="skor-penilaian"
                    />
              </CInputGroup>
              {
                buttonSimpanTambah === false &&
                <CButton onClick={handleTambahData} style={{marginRight:10}} color="success">Simpan</CButton> 
              }
          </CCol>
        </CRow>
        </CCard>
  
      }
 

    </CContainer>
      )
  }

  export default RiwayatKinerja