import React, { useState, useEffect } from "react";
import {
    CCol,
    CRow,
    CContainer,
    CFormGroup,
    CSelect,
    CInput,
    CCard,
    CButton,
    CButtonToolbar,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
  } from "@coreui/react";
  import axios from "axios";

  const RiwayatAnak = () =>{

    const [formEditData,setFormEditData] = useState(false);
    const [formAddData,setFormAddData] = useState(false);

    const [id,setId] = useState("");
    const [nik] = useState(localStorage.getItem('nik'))
    const [namaLengkap,setNamaLengkap] = useState("");
    const [tempatLahir,setTempatLahir ] = useState("");
    const [tanggalLahir,setTanggalLahir] = useState("");
    const [jenisKelamin,setJenisKelamin] = useState("");
    const [status,setStatus] = useState("");
    const [anakKe,setAnakKe] = useState("");
    const [pendidikan,setPendidikan] = useState("");

// ----

    const [namaLengkapTambah,setNamaLengkapTambah] = useState("");
    const [tempatLahirTambah,setTempatLahirTambah ] = useState("");
    const [tanggalLahirTambah,setTanggalLahirTambah] = useState("");
    const [jenisKelaminTambah,setJenisKelaminTambah] = useState("");
    const [statusTambah,setStatusTambah] = useState("");
    const [anakKeTambah,setAnakKeTambah] = useState("");
    const [pendidikanTambah,setPendidikanTambah] = useState("");

    const [data,setData] = useState([])
    const [buttonTambah,setButtonSimpanAdd] = useState(true)

    useEffect(()=>{
      axios.get(""+window.server+"rest-api-sip/profile/user/riwayatanak/getanak.php?nik="+nik+"")
        .then((res)=>{
          if (res.data.message === "No post found") {
            setData([])
          } else {
            setData(res.data)
          }
        })
        .catch((err)=>{
          console.log(err)
        })

    },[])

    const handleNamaLengkap = e=>{
      setNamaLengkap(e.target.value)
    }

    const handleTempatLahir = e =>{
      setTempatLahir(e.target.value)
    }

    const handleTanggalLahir = e =>{
      setTanggalLahir(e.target.value)
    }

    const handleJenisKelamin = e =>{
      setJenisKelamin(e.target.value)
    }

    const handleStatus = e =>{
      setStatus(e.target.value)
    }

    const handleAnakKe = e =>{
      setAnakKe(e.target.value)
    }

    const handlePendidikan = e =>{
      setPendidikan(e.target.value)
    }

    // ------------

    const handleNamaLengkapTambah = e=>{
      setNamaLengkapTambah(e.target.value)
    }

    const handleTempatLahirTambah = e =>{
      setTempatLahirTambah(e.target.value)
    }

    const handleTanggalLahirTambah = e =>{
      setTanggalLahirTambah(e.target.value)
    }

    const handleJenisKelaminTambah = e =>{
      setJenisKelaminTambah(e.target.value)
    }

    const handleStatusTambah = e =>{
      setStatusTambah(e.target.value)
    }

    const handleAnakKeTambah = e =>{
      setAnakKeTambah(e.target.value)
    }

    const handlePendidikanTambah = e =>{
      setPendidikanTambah(e.target.value)
    }

    const handleUbahData = () =>{
      var post = {
        id: id,
        nik: nik,
        nama_lengkap: namaLengkap,
        tempat_lahir: tempatLahir,
        tanggal_lahir: tanggalLahir,
        jenis_kelamin: jenisKelamin,
        status: status,
        anak_ke: anakKe,
        pendidikan: pendidikan
  
      }
      axios.put(""+window.server+"rest-api-sip/profile/user/riwayatanak/updateanak.php",post)
        .then((res)=>{
          setFormEditData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err)=>{
          console.log(err)
        })
    }

    const handleTambahData = () =>{
      setButtonSimpanAdd(false)
      var post = {
        nik: nik,
        nama_lengkap: namaLengkapTambah,
        tempat_lahir: tempatLahirTambah,
        tanggal_lahir: tanggalLahirTambah,
        jenis_kelamin: jenisKelaminTambah,
        status: statusTambah,
        anak_ke: anakKeTambah,
        pendidikan: pendidikanTambah
      }

      if (namaLengkapTambah === "") {
        setButtonSimpanAdd(true)
        alert("Nama lengkap tidak boleh kosong")
      } else if (tempatLahirTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tempat lahir tidak boleh kosong")
      } else if (tanggalLahirTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tanggal lahir tidak boleh kosong")
      } else if (jenisKelaminTambah === "") {
        setButtonSimpanAdd(true)
        alert("Jenis kelamin tidak boleh kosong")
      }else if (statusTambah === "") {
        setButtonSimpanAdd(true)
        alert("Status tidak boleh kosong")
      }else if (anakKeTambah === "") {
        setButtonSimpanAdd(true)
        alert("Anak ke tidak boleh kosong")
      }else if (pendidikanTambah === "") {
        setButtonSimpanAdd(true)
        alert("Pendidikan tidak boleh kosong")
      } else{
        axios.post(""+window.server+"rest-api-sip/profile/user/riwayatanak/insertanak.php",post)
        .then((res)=>{
          if (res.data.message === "Data Inserted") {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          } else {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          }
        })
        .catch((err)=>{
          setButtonSimpanAdd(true)
          console.log(err)
        })
      }
    }

    const handleHapusData = e => {
      if (window.confirm('Apakah anda yakin ?')) {
        axios
          .delete("" + window.server + "rest-api-sip/profile/user/riwayatanak/deleteanak.php", {
            data: { id: e.target.value },
          })
          .then((res) => {
            alert(JSON.stringify(res.data))
            window.location.reload();
          })
          .catch((err) => {
            console.log(err)
          })
      }
  
    }
    

      return(
    <CContainer>
             <CCard style={{padding:20}}>
        <CButtonToolbar justify="end">
        <CButton onClick={()=>{
          setFormAddData(true)
          setFormEditData(false)
        }} style={{marginRight:10}} color="success">Tambah</CButton>
       
      </CButtonToolbar>
      <CRow>
    <CCol  className="py-3">
<table>
  <tr>
    <th style={{width:'3%'}}>No</th>
    <th style={{width:'5%'}}>Nama Lengkap </th>
    <th style={{width:'5%'}}>Tempat Lahir</th>
    <th style={{width:'5%'}}>Tanggal Lahir</th>
    <th style={{width:'5%'}}>Jenis Kelamin</th>
    <th style={{width:'5%'}}>Status</th>
    <th style={{width:'5%'}}>Anak ke</th>
    <th style={{width:'5%'}}>Pendidikan</th>
    <th style={{width:'10%'}}>Aksi</th>
  </tr>
    {
      data.map((res,index)=>{
        return(
          <tr>
            <td>{index + 1}</td>
            <td>{res.nama_lengkap}</td>
            <td>{res.tempat_lahir}</td>
            <td>{res.tanggal_lahir}</td>
            <td>{res.jenis_kelamin}</td>
            <td>{res.status}</td>
            <td>{res.anak_ke}</td>
            <td>{res.pendidikan}</td>
            
          <td>
            <CButton onClick={()=>{
              setFormEditData(true)
              setFormAddData(false)
              setNamaLengkap(res.nama_lengkap)
              setTempatLahir(res.tempat_lahir)
              setTanggalLahir(res.tanggal_lahir)
              setJenisKelamin(res.jenis_kelamin)
              setStatus(res.status)
              setAnakKe(res.anak_ke)
              setPendidikan(res.pendidikan)
              setId(res.id)
            
            }} style={{marginRight:10}} color="info">Ubah</CButton>
              <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>
          </td>
          </tr>
        )
      })
    }

 
</table>
        </CCol>
        
      </CRow>
  
      </CCard>

      {
        formEditData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Ubah Data</h3>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Lengkap
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleNamaLengkap}
                      value={namaLengkap}
                      type="text"
                      placeholder="Nama Lengkap"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tempat Lahir
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tempatLahir}
                      onChange={handleTempatLahir}
                      type="text"
                      placeholder="Tempat Lahir"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Lahir
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalLahir}
                      onChange={handleTanggalLahir}
                      type="date"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Jenis Kelamin
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={jenisKelamin}
                      onChange={handleJenisKelamin}
                      type="text"
                      placeholder="Jenis Kelamin"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Status
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={status}
                      onChange={handleStatus}
                      type="text"
                      placeholder="Status"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Anak Ke
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={anakKe}
                      onChange={handleAnakKe}
                      type="number"
                      placeholder="Anak Ke"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Pendidikan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={pendidikan}
                      onChange={handlePendidikan}
                      type="text"
                      placeholder="Pendidikan"
                    />
              </CInputGroup>
              <CButton onClick={handleUbahData} style={{marginRight:10}} color="success">Simpan</CButton>
          </CCol>
        </CRow>
        </CCard>
  
      }

      {
        formAddData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Tambah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Lengkap
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleNamaLengkapTambah}
                      value={namaLengkapTambah}
                      type="text"
                      placeholder="Nama Lengkap"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tempat Lahir
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tempatLahirTambah}
                      onChange={handleTempatLahirTambah}
                      type="text"
                      placeholder="Tempat Lahir"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Lahir
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalLahirTambah}
                      onChange={handleTanggalLahirTambah}
                      type="date"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Jenis Kelamin
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CSelect  onChange={handleJenisKelaminTambah}>
                      <option value="">-- Pilih --</option>
                      <option value="Laki-Laki">Laki-Laki</option>
                      <option value="Perempuan">Permepuan</option>
                    </CSelect>
                   
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Status
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CSelect  onChange={handleStatusTambah}>
                    <option value="">-- Pilih --</option>
                      <option value="Kandung">Kandung</option>
                      <option value="Angkat">Angkat</option>
                    </CSelect>
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Anak Ke
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={anakKeTambah}
                      onChange={handleAnakKeTambah}
                      type="number"
                      placeholder="Anak Ke"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Pendidikan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={pendidikanTambah}
                      onChange={handlePendidikanTambah}
                      type="text"
                      placeholder="Pendidikan"
                    />
              </CInputGroup>
              {
                buttonTambah === true &&
                <CButton onClick={handleTambahData} style={{marginRight:10}} color="success">Simpan</CButton>
              }
          </CCol>
        </CRow>
        </CCard>
  
      }
 

    </CContainer>
      )
  }

  export default RiwayatAnak