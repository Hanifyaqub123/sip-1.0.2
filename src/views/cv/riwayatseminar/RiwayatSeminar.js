import React, { useState, useEffect } from "react";
import {
    CCol,
    CRow,
    CContainer,
    CFormGroup,
    CLabel,
    CInput,
    CCard,
    CButton,
    CButtonToolbar,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
  } from "@coreui/react";
  import axios from "axios";

  const RiwayatSeminar = () =>{

    const [formEditData,setFormEditData] = useState(false);
    const [formAddData,setFormAddData] = useState(false);

    const [id,setId] = useState("")
    const [nik] = useState(localStorage.getItem('nik'))
    const [namaDiklat,setNamaDiklat] = useState("")
    const [tahun,setTahun] = useState("")
    const [tanggalMulai,setTanggalMulai] = useState("")
    const [tanggalSelesai,setTanggalSelesai] = useState("")
    const [tempat,setTempat] = useState("")
    const [penyelenggara,setPenyelenggara] = useState("")
    const [noSertipikat,setNoSertipikat] = useState("")
    const [tanggalSertipikat,setTanggalSertipikat] = useState("")

    const [namaDiklatTambah,setNamaDiklatTambah] = useState("")
    const [tahunTambah,setTahunTambah] = useState("")
    const [tanggalMulaiTambah,setTanggalMulaiTambah] = useState("")
    const [tanggalSelesaiTambah,setTanggalSelesaiTambah] = useState("")
    const [tempatTambah,setTempatTambah] = useState("")
    const [penyelenggaraTambah,setPenyelenggaraTambah] = useState("")
    const [noSertipikatTambah,setNoSertipikatTambah] = useState("")
    const [tanggalSertipikatTambah,setTanggalSertipikatTambah] = useState("")



    useEffect(()=>{
      axios.get(""+window.server+"rest-api-sip/profile/user/diklat/getdiklat.php?nik="+nik+"")
        .then((res)=>{
          if (res.data.message === "No post found") {
              setData([])
          } else {
            setData(res.data)
          }
        })
        .catch((err)=>{
          console.log(err)
        })
    },[])


    const [data,setData] = useState([]);
    const [buttonSimpanAdd,setButtonSimpanAdd] = useState(true)

    const handleNamaDiklat = e =>{
      setNamaDiklat(e.target.value)
    }
    const handleTahun = e =>{
      setTahun(e.target.value)
    }
    const handleTanggalMulai = e =>{
      setTanggalMulai(e.target.value)
    }
    const handleTanggalSelesai = e =>{
      setTanggalSelesai(e.target.value)
    }
    const handleTempat = e =>{
      setTempat(e.target.value)
    }
    const handlePenyelenggara = e =>{
      setPenyelenggara(e.target.value)
    }
    const handleNoSertipikat = e =>{
      setNoSertipikat(e.target.value)
    }
    const handleTanggalSertipikat = e =>{
      setTanggalSertipikat(e.target.value)
    }

    // ------------------------
    
    const handleNamaDiklatTambah = e =>{
      setNamaDiklatTambah(e.target.value)
    }
    const handleTahunTambah = e =>{
      setTahunTambah(e.target.value)
    }
    const handleTanggalMulaiTambah = e =>{
      setTanggalMulaiTambah(e.target.value)
    }
    const handleTanggalSelesaiTambah = e =>{
      setTanggalSelesaiTambah(e.target.value)
    }
    const handleTempatTambah = e =>{
      setTempatTambah(e.target.value)
    }
    const handlePenyelenggaraTambah = e =>{
      setPenyelenggaraTambah(e.target.value)
    }
    const handleNoSertipikatTambah = e =>{
      setNoSertipikatTambah(e.target.value)
    }
    const handleTanggalSertipikatTambah = e =>{
      setTanggalSertipikatTambah(e.target.value)
    }

    const handleUbahData = () =>{
      var post = {
        id: id,
        nik: nik,
        nama_diklat: namaDiklat,
        tahun: tahun,
        tanggal_mulai: tanggalMulai,
        tanggal_selesai: tanggalSelesai,
        tempat: tempat,
        penyelenggara: penyelenggara,  
        no_sertipikat: noSertipikat,
        tanggal_sertipikat: tanggalSertipikat,
      }
      axios.put(""+window.server+"rest-api-sip/profile/user/diklat/updatediklat.php",post)
        .then((res)=>{
          setFormEditData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err)=>{
          console.log(err)
        })
    }

    const handleTambahData = () =>{
      setButtonSimpanAdd(false)
      var post = {
        id: id,
        nik: nik,
        nama_diklat: namaDiklatTambah,
        tahun: tahunTambah,
        tanggal_mulai: tanggalMulaiTambah,
        tanggal_selesai: tanggalSelesaiTambah,
        tempat: tempatTambah,
        penyelenggara: penyelenggaraTambah,  
        no_sertipikat: noSertipikatTambah,
        tanggal_sertipikat: tanggalSertipikatTambah,
      }
      if (namaDiklatTambah === "") {
        setButtonSimpanAdd(true)
        alert("Nama diklat tidak boleh kosong")
      } else if (tahunTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tahun tidak boleh kosong")
      } else if (tanggalMulaiTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tanggal mulai tidak boleh kosong")
      } else if (tanggalSelesaiTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tanggal selesai tidak boleh kosong")
      }else if (tempatTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tempat tidak boleh kosong")
      }else if (penyelenggaraTambah === "") {
        setButtonSimpanAdd(true)
        alert("Penyelenggara tidak boleh kosong")
      } else if (noSertipikatTambah === "") {
        setButtonSimpanAdd(true)
        alert("Nomor sertipikat tidak boleh kosong")
      }else if (tanggalSertipikatTambah === "") {
        setButtonSimpanAdd(true)
        alert("Tanggal sertipikat tidak boleh kosong")
      }else{
        axios.post(""+window.server+"rest-api-sip/profile/user/diklat/insertdiklat.php",post)
        .then((res)=>{
          if (res.data.message === "Data Inserted") {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          } else {
            setFormAddData(false)
            setButtonSimpanAdd(true)
            alert(JSON.stringify(res.data))
            window.location.reload();
          }
        })
        .catch((err)=>{
          setButtonSimpanAdd(true)
          console.log(err)
        })
      }
    }

    
    const handleHapusData = e => {
      if (window.confirm('Apakah anda yakin ?')) {
        axios
          .delete("" + window.server + "rest-api-sip/profile/user/diklat/deletediklat.php", {
            data: { id: e.target.value },
          })
          .then((res) => {
            alert(JSON.stringify(res.data))
            window.location.reload();
          })
          .catch((err) => {
            console.log(err)
          })
      }
  
    }
    
      return(
    <CContainer>
             <CCard style={{padding:20}}>
        <CButtonToolbar justify="end">
        <CButton onClick={()=>{
          setFormAddData(true)
          setFormEditData(false)
        }} style={{marginRight:10}} color="success">Tambah</CButton>
       
      </CButtonToolbar>
      <CRow>
    <CCol  className="py-3">
<table>
  <tr>
    <th style={{width:'3%'}}>No</th>
    <th style={{width:'3%'}}>Nama Diklat/Seminar</th>
    <th style={{width:'5%'}}>Tahun </th>
    <th style={{width:'5%'}}>Tanggal Mulai</th>
    <th style={{width:'5%'}}>Tanggal Selesai</th>
    <th style={{width:'5%'}}>Tempat</th>
    <th style={{width:'5%'}}>Penyelenggara</th>
    <th style={{width:'5%'}}>No Sertipikat</th>
    <th style={{width:'5%'}}>Tanggal Sertipikat</th>
    <th style={{width:'15%'}}>Aksi</th>
  </tr>
    {
      data.map((res,index)=>{
        return(
          <tr>
            <td>{index+1}</td>
            <td>{res.nama_diklat}</td>
            <td>{res.tahun}</td>
            <td>{res.tanggal_mulai}</td>
            <td>{res.tanggal_selesai}</td>
            <td>{res.tempat}</td>
            <td>{res.penyelenggara}</td>
            <td>{res.no_sertipikat}</td>
            <td>{res.tanggal_sertipikat}</td>
          <td>
            <CButton onClick={()=>{
              setFormEditData(true)
              setFormAddData(false)
              setNamaDiklat(res.nama_diklat)
              setTahun(res.tahun)
              setTanggalMulai(res.tanggal_mulai)
              setTanggalSelesai(res.tanggal_selesai)
              setTempat(res.tempat)
              setPenyelenggara(res.penyelenggara)
              setNoSertipikat(res.no_sertipikat)
              setTanggalSertipikat(res.tanggal_sertipikat)
              setId(res.id)
             
            }} style={{marginRight:10}} color="info">Ubah</CButton>
              <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>
          </td>
          </tr>
        )
      })
    }

 
</table>
        </CCol>
        
      </CRow>
  
      </CCard>

      {
        formEditData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Ubah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Diklat / Seminar
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleNamaDiklat}
                      value={namaDiklat}
                      type="text"
                      placeholder="Nama Diklat / Seminar"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tahun
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tahun}
                      onChange={handleTahun}
                      type="number"
                      placeholder="Tahun"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Mulai
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalMulai}
                      onChange={handleTanggalMulai}
                      type="date"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Selesai
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalSelesai}
                      onChange={handleTanggalSelesai}
                      type="date"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tempat
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tempat}
                      onChange={handleTempat}
                      type="text"
                      placeholder="Tempat"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Penyelenggara
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={penyelenggara}
                      onChange={handlePenyelenggara}
                      type="text"
                      placeholder="Penyelenggara"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        No Serti[ikat]
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={noSertipikat}
                      onChange={handleNoSertipikat}
                      type="text"
                      placeholder="No Sertipikat"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Sertipikat
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalSertipikat}
                      onChange={handleTanggalSertipikat}
                      type="date"
                    />
              </CInputGroup>
              <CButton onClick={handleUbahData} style={{marginRight:10}} color="success">Simpan</CButton>
          </CCol>
        </CRow>
        </CCard>
  
      }

      {
        formAddData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Tambah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Diklat / Seminar
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleNamaDiklatTambah}
                      value={namaDiklatTambah}
                      type="text"
                      placeholder="Nama Diklat / Seminar"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tahun
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tahunTambah}
                      onChange={handleTahunTambah}
                      type="number"
                      placeholder="Tahun"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Mulai
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalMulaiTambah}
                      onChange={handleTanggalMulaiTambah}
                      type="date"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Selesai
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalSelesaiTambah}
                      onChange={handleTanggalSelesaiTambah}
                      type="date"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tempat
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tempatTambah}
                      onChange={handleTempatTambah}
                      type="text"
                      placeholder="Tempat"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Penyelenggara
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={penyelenggaraTambah}
                      onChange={handlePenyelenggaraTambah}
                      type="text"
                      placeholder="Penyelenggara"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        No Serti[ikat]
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={noSertipikatTambah}
                      onChange={handleNoSertipikatTambah}
                      type="text"
                      placeholder="No Sertipikat"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Sertipikat
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalSertipikatTambah}
                      onChange={handleTanggalSertipikatTambah}
                      type="date"
                    />
              </CInputGroup>
              {
                buttonSimpanAdd === true &&
                <CButton onClick={handleTambahData} style={{marginRight:10}} color="success">Simpan</CButton>
              }
          </CCol>
        </CRow>
        </CCard>
  
      }
 

    </CContainer>
      )
  }

  export default RiwayatSeminar