import React, { useState, useEffect } from "react";
import {
    CCol,
    CRow,
    CContainer,
    CFormGroup,
    CSelect,
    CInput,
    CCard,
    CButton,
    CButtonToolbar,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
  } from "@coreui/react";
  import axios from "axios";

  const RiwayatSuamiIstri = () =>{

    const [formEditData,setFormEditData] = useState(false);
    const [formAddData,setFormAddData] = useState(false);
    const [id,setId] = useState("");
    const [nik] = useState(localStorage.getItem('nik'))

    const [namaSuamiIstri,setNamaSuamiIstri] = useState("");
    const [status,setStatus] = useState("");
    const [suamiIstriKe,setSuamiIstrike] = useState("");
    const [tanggalNikah,setTanggalNikah] = useState("");
    const [pekerjaan,setPekerjaan] = useState("");

    const [namaSuamiIstriTambah,setNamaSuamiIstriTambah] = useState("");
    const [statusTambah,setStatusTambah] = useState("");
    const [suamiIstriKeTambah,setSuamiIstrikeTambah] = useState("");
    const [tanggalNikahTambah,setTanggalNikahTambah] = useState("");
    const [pekerjaanTambah,setPekerjaanTambah] = useState("");

    const [data,setData] = useState([])
    const [buttonTambah,setButtonTambah] = useState(true)

    useEffect(()=>{
      axios.get(""+window.server+"rest-api-sip/profile/user/riwayatsuamiistri/getsuamiistri.php?nik="+nik+"")
        .then((res)=>{
          if (res.data.message === "No post found") {
            setData([])
          } else {
            setData(res.data)
          }
        })
        .then((err)=>{
          console.log(err)
        })
    },[])

    const handleNamaSuamiIstri = e =>{
      setNamaSuamiIstri(e.target.value)
    }

    const handleStatus = e =>{
      setStatus(e.target.value)
    }

    const handleSuamiIstriKe = e =>{
      setSuamiIstrike(e.target.value)
    }

    const handleTanggalNikah = e =>{
      setTanggalNikah(e.target.value)
    }

    const handlePekerjaan = e =>{
      setPekerjaan(e.target.value)
    }


    // -----------------------

    const handleNamaSuamiIstriTambah = e =>{
      setNamaSuamiIstriTambah(e.target.value)
    }

    const handleStatusTambah = e =>{
      setStatusTambah(e.target.value)
    }

    const handleSuamiIstriKeTambah = e =>{
      setSuamiIstrikeTambah(e.target.value)
    }

    const handleTanggalNikahTambah = e =>{
      setTanggalNikahTambah(e.target.value)
    }

    const handlePekerjaanTambah = e =>{
      setPekerjaanTambah(e.target.value)
    }

    const handleUbahData = () =>{
      var post = {
        id : id,
        nik : nik,
        nama_suami_istri : namaSuamiIstri,
        status : status,
        suami_istri_ke : suamiIstriKe,
        tanggal_nikah : tanggalNikah,
        pekerjaan : pekerjaan
      }
      axios.put(""+window.server+"rest-api-sip/profile/user/riwayatsuamiistri/updatesuamiistri.php",post)
        .then((res)=>{
          setFormEditData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err)=>{
          console.log(err)
        })
    }

    const handleTambahData = () =>{
      setButtonTambah(false)
      var post = {
        nik : nik,
        nama_suami_istri : namaSuamiIstriTambah,
        status : statusTambah,
        suami_istri_ke : suamiIstriKeTambah,
        tanggal_nikah : tanggalNikahTambah,
        pekerjaan : pekerjaanTambah
      }
      
      if (namaSuamiIstriTambah === "") {
          setButtonTambah(true)
          alert("Nama Suami / Istri tidak boleh kosong")
      } else  if (statusTambah === "") {
        setButtonTambah(true)
        alert("Status tidak boleh kosong")
      }else  if (suamiIstriKeTambah === "") {
        setButtonTambah(true)
        alert("Suami Istri Ke  tidak boleh kosong")
      }else  if (tanggalNikahTambah === "") {
        setButtonTambah(true)
        alert("Tanggal Nikah tidak boleh kosong")
      }else  if (pekerjaanTambah === "") {
        setButtonTambah(true)
        alert("Pekerjaan tidak boleh kosong")
      } else {
        axios.post(""+window.server+"rest-api-sip/profile/user/riwayatsuamiistri/insertsuamiistri.php",post)
          .then((res)=>{
            setButtonTambah(true)
            setFormAddData(false)
            alert(JSON.stringify(res.data))
            window.location.reload();
          })
          .catch((err)=>{
            console.log(err)
            setButtonTambah(true)
          })
      }

     
    }
    const handleHapusData = e => {
      if (window.confirm('Apakah anda yakin ?')) {
        axios
          .delete("" + window.server + "rest-api-sip/profile/user/riwayatsuamiistri/deletesuamiistri.php", {
            data: { id: e.target.value },
          })
          .then((res) => {
            alert(JSON.stringify(res.data))
            window.location.reload();
          })
          .catch((err) => {
            console.log(err)
          })
      }
  
    }
    

      return(
    <CContainer>
             <CCard style={{padding:20}}>
        <CButtonToolbar justify="end">
        <CButton onClick={()=>{
          setFormAddData(true)
          setFormEditData(false)
        }} style={{marginRight:10}} color="success">Tambah</CButton>
       
      </CButtonToolbar>
      <CRow>
    <CCol  className="py-3">
<table>
  <tr>
    <th style={{width:'3%'}}>No</th>
    <th style={{width:'10%'}}>Nama Suami / Istri</th>
    <th style={{width:'10%'}}>Status</th>
    <th style={{width:'10%'}}>Suami Istri ke -</th>
    <th style={{width:'10%'}}>Tanggal Nikah</th>
    <th style={{width:'10%'}}>Pekerjaan</th>
    <th style={{width:'15%'}}>Aksi</th>
  </tr>
    {
      data.map((res,index)=>{
        return(
          <tr>
            <td>{index+1}</td>
            <td>{res.nama_suami_istri}</td>
            <td>{res.status}</td>
            <td>{res.suami_istri_ke}</td>
            <td>{res.tanggal_nikah}</td>
            <td>{res.pekerjaan}</td>
          <td>
            <CButton onClick={()=>{
              setFormEditData(true)
              setFormAddData(false)
              setNamaSuamiIstri(res.nama_suami_istri)
              setStatus(res.status)
              setSuamiIstrike(res.suami_istri_ke)
              setTanggalNikah(res.tanggal_nikah)
              setPekerjaan(res.pekerjaan)
              setId(res.id)
            }} style={{marginRight:10}} color="info">Ubah</CButton>
              <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>
          </td>
          </tr>
        )
      })
    }

 
</table>
        </CCol>
        
      </CRow>
  
      </CCard>

      {
        formEditData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Ubah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Suami / RiwayatSuamiIstri
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      onChange={handleNamaSuamiIstri}
                      value={namaSuamiIstri}
                      type="text"
                      placeholder="Nama Suami / Istri"
                      autoComplete="nama-suami-istri"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Status
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CSelect  onChange={handleStatus}>
                      <option value={status}>{status}</option>
                      <option value="Kawin">Kawin</option>
                      <option value="Cerai">Cerai</option>
                    </CSelect>
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Suami Istri ke -
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={suamiIstriKe}
                      onChange={handleSuamiIstriKe}
                      type="number"
                      placeholder="Suami Istri ke -"
                      autoComplete="suami-istri-ke"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Nikah
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalNikah}
                      onChange={handleTanggalNikah}
                      type="date"
                      placeholder="Tanggal Nikah"
                      autoComplete="tanggal-nikah"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Pekerjaan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={pekerjaan}
                      onChange={handlePekerjaan}
                      type="text"
                      placeholder="Pekerjaan"
                      autoComplete="pekerjaan"
                    />
              </CInputGroup>
              <CButton onClick={handleUbahData} style={{marginRight:10}} color="success">Simpan</CButton>
          </CCol>
        </CRow>
        </CCard>
  
      }

      {
        formAddData === true &&
        <CCard>
        <CRow style={{padding:20}}>
          <CCol lg="12" className="py-3">
            <h3>Tambah Data</h3>
            <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Nama Suami / Istri
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={namaSuamiIstriTambah}
                      onChange={handleNamaSuamiIstriTambah}
                      type="text"
                      placeholder="Nama Suami Istri"
                      autoComplete="nama-suami-istri"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Status
                      </CInputGroupText>
                    </CInputGroupPrepend>
                      
                    <CSelect  onChange={handleStatusTambah}>
                      <option value="">-- Pilih --</option>
                      <option value="Cerai">Cerai</option>
                      <option value="Kawin">Kawin</option>
                    </CSelect>

              </CInputGroup>
             <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Suami Istri Ke -
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={suamiIstriKeTambah}
                      onChange={handleSuamiIstriKeTambah}
                      type="number"
                      placeholder="Suami Istri Ke -"
                      autoComplete="suami-istri-ke"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Tanggal Nikah
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={tanggalNikahTambah}
                      onChange={handleTanggalNikahTambah}
                      type="date"
                      autoComplete="tanggal-nikah"
                    />
              </CInputGroup>
              <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        Pekerjaan
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput
                      value={pekerjaanTambah}
                      onChange={handlePekerjaanTambah}
                      type="text"
                      placeholder="Pekerjaan"
                    />
              </CInputGroup>
              {
                buttonTambah === true &&
                <CButton onClick={handleTambahData} style={{marginRight:10}} color="success">Simpan</CButton>

              }
          </CCol>
        </CRow>
        </CCard>
  
      }
 

    </CContainer>
      )
  }

  export default RiwayatSuamiIstri