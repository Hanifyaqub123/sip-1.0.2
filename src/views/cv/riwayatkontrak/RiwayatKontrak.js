import React, { useState, useEffect } from "react";
import {
  CCol,
  CRow,
  CContainer,
  CInput,
  CCard,
  CButton,
  CButtonToolbar,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CSpinner
} from "@coreui/react";
import axios from "axios";

const RiwayatKontrak = () => {

  const [formEditData, setFormEditData] = useState(false);
  const [formAddData, setFormAddData] = useState(false);

  const [nik] = useState(localStorage.getItem("nik"))
  const [id, setId] = useState("");
  const [namaJabatan, setNamaJabatan] = useState("");
  const [nomorKontrak, setNomorKontrak] = useState("");
  const [masaBerlaku, setMasaBerlaku] = useState("");
  const [gaji, setGaji] = useState("");
  const [unitKerja, setUnitKerja] = useState("");

  const [namaJabatanTambah, setNamaJabatanTambah] = useState("");
  const [nomorKontrakTambah, setNomorKontrakTambah] = useState("");
  const [masaBerlakuTambah, setMasaBerlakuTambah] = useState("");
  const [gajiTambah, setGajiTambah] = useState("");
  const [unitKerjaTambah, setUnitKerjaTambah] = useState("Kantah Kota Salatiga");

  const [data, setData] = useState([])
  const [buttonSimpanAdd, setButtonSimpanAdd] = useState(true)

  useEffect(() => {
    axios.get("" + window.server + "rest-api-sip/profile/user/riwayatkontrak/getkontrak.php?nik=" + nik + "")
      .then((res) => {
        if (res.data.message === "No post found") {
          setData([])
        } else {
          setData(res.data)
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }, [])

  const handleNamaJabatan = e => {
    setNamaJabatan(e.target.value)
  }
  const handleNomorKontrak = e => {
    setNomorKontrak(e.target.value)
  }
  const handleMasaBerlaku = e => {
    setMasaBerlaku(e.target.value)
  }
  const handleGaji = e => {
    setGaji(e.target.value)
  }
  const handleUnitKerja = e => {
    setUnitKerja(e.target.value)
  }
  // -----------------------------------------
  const handleNamaJabatanTambah = e => {
    setNamaJabatanTambah(e.target.value)
  }
  const handleNomorKontrakTambah = e => {
    setNomorKontrakTambah(e.target.value)
  }
  const handleMasaBerlakuTambah = e => {
    setMasaBerlakuTambah(e.target.value)
  }
  const handleGajiTambah = e => {
    setGajiTambah(e.target.value)
  }
  const handleUnitKerjaTambah = e => {
    setUnitKerjaTambah(e.target.value)
  }

  const handleUbahData = () => {

    var pos = {
      id: id,
      nik: nik,
      nama_jabatan: namaJabatan,
      no_kontrak: nomorKontrak,
      masa_berlaku: masaBerlaku,
      unit_kerja: unitKerja
    }
    axios.post("" + window.server + "rest-api-sip/profile/user/riwayatkontrak/updatekontrak.php", pos)
      .then((res) => {
        alert(JSON.stringify(res.data))
        window.location.reload();
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const handleTambahData = () => {
    setButtonSimpanAdd(false)
    var pos = {
      nik: nik,
      nama_jabatan: namaJabatanTambah,
      no_kontrak: nomorKontrakTambah,
      masa_berlaku: masaBerlakuTambah,
      gaji: gajiTambah,
      unit_kerja: unitKerjaTambah
    }

    if (namaJabatanTambah === "") {
      setButtonSimpanAdd(true)
      alert("Nama jabatan tidak boleh kosong")
    } else if (nomorKontrakTambah === "") {
      setButtonSimpanAdd(true)
      alert("Nomor kontrak tidak boleh kosong")
    } else if (masaBerlakuTambah === "") {
      setButtonSimpanAdd(true)
      alert("Masa berlaku tidak boleh kosong")
    } else if (unitKerjaTambah === "") {
      setButtonSimpanAdd(true)
      alert("Unit Kerja tidak boleh kosong")
    } else {
      axios.post("" + window.server + "rest-api-sip/profile/user/riwayatkontrak/insertkontrak.php", pos)
        .then((res) => {
          setButtonSimpanAdd(true)
          setFormAddData(false)
          setFormEditData(false)
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err) => {
          console.log(err)
        })

    }

  }

  const handleHapusData = e => {
    if (window.confirm('Apakah anda yakin ?')) {
      axios
        .delete("" + window.server + "rest-api-sip/profile/user/riwayatkontrak/deletekontrak.php", {
          data: { id: e.target.value },
        })
        .then((res) => {
          alert(JSON.stringify(res.data))
          window.location.reload();
        })
        .catch((err) => {
          console.log(err)
        })
    }

  }

  return (
    <CContainer>
      <CCard style={{ padding: 20 }}>
        <CButtonToolbar justify="end">
          <CButton onClick={() => {
            setFormAddData(true)
            setFormEditData(false)
          }} style={{ marginRight: 10 }} color="success">Tambah</CButton>

        </CButtonToolbar>
        <CRow>
          <CCol className="py-3">
            <table>
              <tr>
                <th style={{ width: '3%' }}>No</th>
                <th style={{ width: '10%' }}>Nama Jabatan</th>
                <th style={{ width: '10%' }}>Nomor Kontrak</th>
                <th style={{ width: '10%' }}>Masa Berlaku</th>
                <th style={{ width: '10%' }}>Gaji</th>
                <th style={{ width: '10%' }}>Unit Kerja</th>
                <th style={{ width: '15%' }}>Aksi</th>
              </tr>
              {
                data.map((res, index) => {
                  return (
                    <tr>
                      <td>{index + 1}</td>
                      <td>{res.nama_jabatan}</td>
                      <td>{res.no_kontrak}</td>
                      <td>{res.masa_berlaku}</td>
                      <td>{res.gaji}</td>
                      <td>{res.unit_kerja}</td>
                      <td>
                        <CButton onClick={() => {
                          setFormEditData(true)
                          setFormAddData(false)
                          setNamaJabatan(res.nama_jabatan)
                          setNomorKontrak(res.no_kontrak)
                          setMasaBerlaku(res.masa_berlaku)
                          setGaji(res.gaji)
                          setId(res.id)
                          setUnitKerja(res.unit_kerja)

                        }} style={{ marginRight: 10 }} color="info">Ubah</CButton>
                        <CButton value={res.id} onClick={handleHapusData} color="danger">Hapus</CButton>

                      </td>
                    </tr>
                  )
                })
              }


            </table>
          </CCol>

        </CRow>

      </CCard>

      {
        formEditData === true &&
        <CCard>
          <CRow style={{ padding: 20 }}>
            <CCol lg="12" className="py-3">
              <h3>Ubah Data</h3>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nama Jabatan
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  onChange={handleNamaJabatan}
                  value={namaJabatan}
                  type="text"
                  placeholder="Nama Jabatan"
                  autoComplete="nama-jabatan"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nomor Kontrak
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={nomorKontrak}
                  onChange={handleNomorKontrak}
                  type="number"
                  autoComplete="nomor-kontrak"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Masa Berlaku
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={masaBerlaku}
                  onChange={handleMasaBerlaku}
                  type="date"
                  placeholder="Masa Berlaku"
                  autoComplete="masa-berlaku"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Gaji
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={gaji}
                  onChange={handleGaji}
                  type="number"
                  placeholder="Gaji"
                  autoComplete="gaji"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Unit Kerja
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  disabled
                  value={unitKerja}
                  onChange={handleUnitKerja}
                  type="text"
                  placeholder="Unit Kerja"
                  autoComplete="unit-kerja"
                />
              </CInputGroup>
              <CButton onClick={() => {
                handleUbahData()
              }} style={{ marginRight: 10 }} color="success">Simpan</CButton>
            </CCol>
          </CRow>
        </CCard>

      }

      {
        formAddData === true &&
        <CCard>
          <CRow style={{ padding: 20 }}>
            <CCol lg="12" className="py-3">
              <h3>Tambah Data</h3>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nama Jabatan
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={namaJabatanTambah}
                  onChange={handleNamaJabatanTambah}
                  type="text"
                  placeholder="Nama Jabatan"
                  autoComplete="nama-jabatan"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Nomor Kontrak
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={nomorKontrakTambah}
                  onChange={handleNomorKontrakTambah}
                  placeholder="Nomor Kontrak"
                  type="number"
                  autoComplete="nomor-kontrak"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Masa Berlaku
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={masaBerlakuTambah}
                  onChange={handleMasaBerlakuTambah}
                  type="date"
                  autoComplete="masa-berlaku"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Gaji
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  value={gajiTambah}
                  onChange={handleGajiTambah}
                  type="number"
                  placeholder="Gaji"
                  autoComplete="gaji"
                />
              </CInputGroup>
              <CInputGroup className="mb-3">
                <CInputGroupPrepend>
                  <CInputGroupText>
                    Unit Kerja
                  </CInputGroupText>
                </CInputGroupPrepend>
                <CInput
                  disabled
                  value={unitKerjaTambah}
                  onChange={handleUnitKerjaTambah}
                  type="text"
                  placeholder="Unit Kerja"
                  autoComplete="unit-kerja"
                />
              </CInputGroup>
              {
                buttonSimpanAdd === true &&
                <CButton onClick={handleTambahData} style={{ marginRight: 10 }} color="success">Simpan</CButton>
              }

            </CCol>
          </CRow>
        </CCard>

      }


    </CContainer>
  )
}

export default RiwayatKontrak