import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CSelect,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CFormGroup,
  CLabel,
  CSpinner
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import axios from "axios";
import { useHistory } from 'react-router-dom'
import ImageUploading from "react-images-uploading";



const Register = () => {
  const history = useHistory();
  const [namaLengkap,setNamaLengkap] = useState("")
  const [noTelp,setNoTelp] = useState("")
  const [jenisKelamin,setJenisKelamin] = useState("")
  const [nik, setNik] = useState("");
  const [agama, setAgama] = useState("");
  const [gelarNonAkademisD, setGelarNonAkademisD] = useState("");
  const [gelarNonAkademisB, setGelarNonAkademisB] = useState("");
  const [gelarAkademisD, setGelarAkademisD] = useState("");
  const [gelarAkademisB, setGelarAkademisB] = useState("");
  const [statusPernikahan, setStatusPernikahan] = useState("");
  const [golonganDarah, setGolonganDarah] = useState("");
  const [tempatLahir, setTempatLahir] = useState("");
  const [tanggallahir, setTanggallahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [rt, setRt] = useState("");
  const [rw, setRw] = useState("");
  const [kodePost, setKodePost] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [verifPassword, setVerifPassword] = useState("");
  const [imagePP, setImagePP] = useState("");
  const [pendidikanTerkahir, setPendidiaknTerakhir] = useState("");
  const [formasiJabatan, setFormasiJabatan] = useState("");
  const [nip, setNip] = useState("");
  const [pejabatPenilai] = useState("")
  const [nipPejabat] = useState("")

  const [provinsi, setProvinsi] = useState([]);
  const [provinsiValue, setProvinsiValue] = useState("");

  const [kota, setKota] = useState([]);
  const [kotaValue, setKotaValue] = useState("");

  const [kecamatan, setkecamatan] = useState([]);
  const [kecamatanValue, setkecamatanValue] = useState("");

  const [loadingImage,setLoadingImage] = useState(false)
  const [loading,setLoading] = useState(false)


  useEffect(() => {
    handleProvisni();
  }, []);

  const  handleProvisni = e =>{
    axios
      .get("https://dev.farizdotid.com/api/daerahindonesia/provinsi")
      .then((res) => {
        setProvinsi(res.data.provinsi);
      });
  }

  const handleProvinsiId = (e) => {
   var result = e.target.value

   var nama = result.split(",").pop()
   var id = result.substring(0,2)

    axios
      .get(
        "https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=" +
        id +
          ""
      )
      .then((res) => {
        setKota(res.data.kota_kabupaten);
        setProvinsiValue(nama)
      });
  };

  const handleKotaId = (e) => {
    var result = e.target.value

    var nama = result.split(",").pop()
    var id = result.substring(0,4)

    axios
      .get(
        "https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=" +
         id +
          ""
      )
      .then((res) => {
        setkecamatan(res.data.kecamatan);
        setKotaValue(nama)
      });
  };

  const handleKecamatan = e =>{
    setkecamatanValue(e.target.value)
  }

  const handleNamaLengkap = e =>{
    setNamaLengkap(e.target.value)
  }

  const handleNik = e =>{
    setNik(e.target.value)
  }

  const handleAgama = e =>{
    setAgama(e.target.value)
  }

  const handleGelarNonAkademisDepan = e =>{
    setGelarNonAkademisD(e.target.value)
  }

  const handleGelarNonAkademisBelakang = e =>{
    setGelarNonAkademisB(e.target.value)
  }

  const handleGelarAkademisDepan = e =>{
    setGelarAkademisD(e.target.value)
  }

  const handleGelarAkademisBelakang = e =>{
    setGelarAkademisB(e.target.value)
  }

  const handleStatusPernikahan = e =>{
    setStatusPernikahan(e.target.value)
  }

  const handleGolonganDarah = e =>{
    setGolonganDarah(e.target.value)
  }

  const handleTempatLahir = e =>{
    setTempatLahir(e.target.value)
  }

  const handleTanggalLahir = e =>{
    setTanggallahir(e.target.value)
  }

  const handleAlamat = e =>{
    setAlamat(e.target.value)
  }
  
  const handleRT = e =>{
    setRt(e.target.value)
  }

  const handleRW = e =>{
    setRw(e.target.value)
  }

  const handleKodePost = e =>{
    setKodePost(e.target.value)
  }

  const handlePendidikanTerakhir = e =>{
    setPendidiaknTerakhir(e.target.value)
  }

  const handleFormasiJabatan = e =>{
    setFormasiJabatan(e.target.value)
  }

  const handleNoTelp = e =>{
    setNoTelp(e.target.value)
  }

  const handleNIP = e =>{
    setNip(e.target.value)
  }

  const handleEmail = e =>{
    setEmail(e.target.value)
  }

  const handlePassword = e =>{
    setPassword(e.target.value)
  }

  const handleVerifPassword = e =>{
    setVerifPassword(e.target.value)
  }

  const handleKembali = e =>{
    history.push("/login")
  }

  const handleDaftar = e =>{
    setLoading(true)
    const post = {
      nik : nik,
      nama_lengkap : namaLengkap,
      jenis_kelamin : jenisKelamin,
      gelar_non_akademis_depan : gelarNonAkademisD,
      gelar_akademis_depan : gelarAkademisD,
      status_pernikahan : statusPernikahan,
      alamat : alamat,
      tempat_lahir : tempatLahir,
      rt : rt,
      rw : rw,
      provinsi : provinsiValue,
      kabupaten : kotaValue,
      kecamatan : kecamatanValue,
      kode_pos : kodePost,
      pendidikan_terakhir : pendidikanTerkahir,
      gelar_non_akademis_belakang : gelarNonAkademisB,
      gelar_akademis_belakang : gelarAkademisB,
      agama : agama,
      tanggal_lahir : tanggallahir,
      golongan_darah : golonganDarah,
      formasi_jabatan : formasiJabatan,
      no_telp : noTelp,
      nip : nip,
      email : email,
      password : password,
      image : imagePP,
      pejabat_penilai : pejabatPenilai,
      nip_pejabat : nipPejabat
    }
    if (namaLengkap === "") {
      setLoading(false)
      alert("Nama Lengkap tidak boleh kosong")
    } else if (jenisKelamin === "") {
      setLoading(false)
      alert("Jenis kelamin tidak boleh kosong")
    } else if (nik === "") {
      setLoading(false)
      alert("NIK tidak boleh kosong")
    } else  if (namaLengkap === "") {
      setLoading(false)
      alert("Nama Lengkap tidak boleh kosong")
    } else  if (gelarNonAkademisD === "") {
      setLoading(false)
      alert("Gelar non akademis depan tidak boleh kosong, isi - ")
    } else  if (gelarAkademisD === "") {
      setLoading(false)
      alert("Gelar akademis depan tidak boleh kosong, isi -")
    } else  if (statusPernikahan === "") {
      setLoading(false)
      alert("Status pernikahan tidak boleh kosong")
    } else  if (alamat === "") {
      setLoading(false)
      alert("Alamat tidak boleh kosong")
    } else  if (tempatLahir === "") {
      setLoading(false)
      alert("Tempat lahir tidak boleh kosong")
    }  else  if (rt === "") {
      setLoading(false)
      alert("RT tidak boleh kosong")
    }  else  if (rw === "") {
      setLoading(false)
      alert("RW tidak boleh kosong")
    } else  if (provinsiValue === "") {
      setLoading(false)
      alert("Provinsi tidak boleh kosong")
    }  else  if (kotaValue === "") {
      setLoading(false)
      alert("Kabupaten / Kota tidak boleh kosong")
    }  else  if (kecamatanValue === "") {
      setLoading(false)
      alert("Kecamatan tidak boleh kosong")
    } else  if (kodePost === "") {
      setLoading(false)
      alert("Kode pos tidak boleh kosong")
    } else  if (pendidikanTerkahir === "") {
      setLoading(false)
      alert("Pendidikan terakhir tidak boleh kosong")
    } else  if (gelarNonAkademisB === "") {
      setLoading(false)
      alert("Gelar non akademis belakang tidak boleh kosong, isi -")
    } else  if (gelarAkademisB === "") {
      setLoading(false)
      alert("Gelar akademis belakang tidak boleh kosong, isi -")
    } else  if (agama === "") {
      setLoading(false)
      alert("Agama tidak boleh kosong")
    } else  if (tanggallahir === "") {
      setLoading(false)
      alert("Tanggal lahir tidak boleh kosong")
    } else  if (golonganDarah === "") {
      setLoading(false)
      alert("Golongan darah tidak boleh kosong")
    } else  if (formasiJabatan === "") {
      setLoading(false)
      alert("Formasi Jabatan tidak boleh kosong")
    } else  if (noTelp === "") {
      setLoading(false)
      alert("Nomor HP tidak boleh kosong")
    } else  if (nip === "") {
      setLoading(false)
      alert("NIP tidak boleh kosong")
    } else  if (email === "") {
      setLoading(false)
      alert("Email tidak boleh kosong")
    } else  if (password === "") {
      setLoading(false)
      alert("Password tidak boleh kosong")
    } else  if (verifPassword === "") {
      setLoading(false)
      alert("Repeat Password tidak boleh kosong")
    } else  if (password !== verifPassword) {
      setLoading(false)
      alert("Password tidak cocok")
    } else if (imagePP === "") {
      setLoading(false)
      alert("Gambar tidak boleh kosong")
    }else{
      axios.post(""+window.server+"rest-api-sip/profile/login/register.php",post)
      .then((res,index)=>{
          setLoading(false)
          alert(JSON.stringify(res.data))
          history.push('/login')
      })
    }
    
  }

 const imageHandler = (imageList) => {
    setLoadingImage(true)
    console.log(imageList[0].file.size /1024 /1024 + "MiB")
    var value = imageList[0].file.size /1024 /1024

    // alert(JSON.stringify(imageList[0].dataURL))
    
    
    if (value < 1.26) {
      setLoadingImage(false)
      setImagePP(imageList[0].dataURL)
      alert("Upload berhasil")
    } else{
        setLoadingImage(false)
        alert("Upss.... ukuran max 1 MB")
    }
  }; 

  const handleJenisKelamin = e =>{
    setJenisKelamin(e.target.value)
  }

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="9" lg="7" xl="10">
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Pendaftaran PPNPN</h1>
                  <p className="text-muted">Mohon isi data dengan benar</p>
                  <h2>Biodata</h2>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                  <CInputGroup className="mb-3">
                  <CInputGroupPrepend>
                    <CInputGroupText>
                    <CIcon name="cil-user" />
                    </CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    value={namaLengkap}
                    type="text"
                    placeholder="Nama Lengkap"
                    autoComplete="nama_lengkap"
                    onChange={handleNamaLengkap}
                  />
                </CInputGroup>
                
                    <CInputGroup  style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                        <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CSelect onChange={handleJenisKelamin}>
                        <option value="" >-- Pilih Jenis Kelamin ---</option>
                        <option value="Laki-Laki" >Laki-Laki</option>
                        <option value="Perempuan" >Perempuan</option>
                      </CSelect>
                    </CInputGroup>

                 
                
                  </div>
                 
                <div style={{ display: "flex", flexDirection: "row" }}>
                    
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={nik}
                        type="number"
                        placeholder="NIK"
                        autoComplete="NIK"
                        onChange={handleNik}
                      />
                    </CInputGroup>

                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <select
                      onChange={handleAgama}
                      class="form-control"
                      >
                        <option value="">-- Pilih Agama --</option>
                        <option value="Islam">Islam</option>
                        <option value="Kristen Protestan">
                          Kristen Protestan
                        </option>
                        <option value="Kristen Katolik">Kristen Katolik</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Buddha">Buddha</option>
                        <option value="Konghucu">Konghucu</option>
                      </select>
                    </CInputGroup>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleGelarNonAkademisDepan}
                        value={gelarNonAkademisD}
                        type="text"
                        placeholder="Gelar Non Akademis Depan"
                        autoComplete="gelar-non-akademis-depan"
                      />
                    </CInputGroup>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={gelarNonAkademisB}
                        onChange={handleGelarNonAkademisBelakang}
                        type="text"
                        placeholder="Gelar Non Akademis Belakang"
                        autoComplete="gelar-non-akademis-belakang"
                      />
                    </CInputGroup>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={gelarAkademisD}
                        onChange={handleGelarAkademisDepan}
                        type="text"
                        placeholder="Gelar Akademis Depan"
                        autoComplete="gelar-akademis-depan"
                      />
                    </CInputGroup>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={gelarAkademisB}
                        onChange={handleGelarAkademisBelakang}
                        type="text"
                        placeholder="Gelar Akademis Belakang"
                        autoComplete="gelar-akademis-belakang"
                      />
                    </CInputGroup>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                        marginRight: 10,
                        width: "48.7%",
                      }}
                    >
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <select
                        onChange={handleStatusPernikahan}
                        value={statusPernikahan}
                        class="form-control"
                      >
                        <option value="">--Status Pernikahan--</option>
                        <option value="Belum Menikah">Belum Menikah</option>
                        <option value="Sudah Menikah">Sudah Menikah</option>
                      </select>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                        marginRight: 10,
                        width: "48.7%",
                      }}
                    >
                      <CInputGroupPrepend>
                        <CInputGroupText>O</CInputGroupText>
                      </CInputGroupPrepend>
                      <select
                        onChange={handleGolonganDarah}
                        value={golonganDarah}
                       class="form-control"
                      >
                        <option value="">--Golongan Darah--</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="AB">AB</option>
                        <option value="O">O</option>
                      </select>
                    </div>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleTempatLahir}
                        value={tempatLahir}
                        type="text"
                        placeholder="Tempat Lahir"
                        autoComplete="tempat-lahir"
                      />
                    </CInputGroup>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                      onChange={handleTanggalLahir}
                        value={tanggallahir}
                        type="date"
                        placeholder="Tanggal Lahir"
                        autoComplete="tanggal-lahir"
                      />
                    </CInputGroup>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleAlamat}
                        value={alamat}
                        type="text"
                        placeholder="Alamat"
                        autoComplete="alamat"
                      />
                    </CInputGroup>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleRT}
                        value={rt}
                        type="number"
                        placeholder="RT"
                        autoComplete="rt"
                      />
                    </CInputGroup>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleRW}
                        value={rw}
                        type="number"
                        placeholder="RW"
                        autoComplete="rw"
                      />
                    </CInputGroup>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                        marginRight: 10,
                        width: "49%",
                      }}
                    >
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <select
                        onChange={handleProvinsiId}
                        class="form-control"
                      >
                        <option value="">--Pilih Provinsi--</option>
                        {provinsi.map((res, index) => {
                          return (
                            <option  key={index} value={[res.id,res.nama]}>
                              {res.nama}
                            </option>
                          );
                        })}
                      </select>
                    </div>

                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                        marginRight: 10,
                        width: "49%",
                      }}
                    >
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <select
                        onChange={handleKotaId}
                        class="form-control"
                      >
                        <option value="">-- Pilih Kabupaten/Kota --</option>
                        {kota.map((res, index) => {
                          return (
                            <option key={index} value={[res.id,res.nama]}>
                              {res.nama}
                            </option>
                          );
                        })}
                      </select>
                    </div>
                  </div>

                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 16,
                        marginRight: 10,
                        width: "100%",
                      }}
                    >
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <select
                        onChange={handleKecamatan}
                        class="form-control"                      >
                        <option value="">-- Pilih Kecamatan --</option>
                        {kecamatan.map((res, index) => {
                          return (
                            <option key={index} value={res.nama}>
                              {res.nama}
                            </option>
                          );
                        })}
                      </select>
                    </div>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleKodePost}
                        value={kodePost}
                        type="text"
                        placeholder="Kode Pos"
                        autoComplete="kode-pos"
                      />
                    </CInputGroup>
                  </div>
                  
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handlePendidikanTerakhir}
                        value={pendidikanTerkahir}
                        type="text"
                        placeholder="Pendidikan Terakhir"
                        autoComplete="pendikan-terakhir"
                      />
                    </CInputGroup>
                    <CInputGroup style={{ marginRight: 10 }} className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        onChange={handleFormasiJabatan}
                        value={formasiJabatan}
                        type="text"
                        placeholder="Formasi Jabatan"
                        autoComplete="formasi-jabatan"
                      />
                    </CInputGroup>
                  </div>
                </CForm>
              </CCardBody>
              <CCardFooter className="p-4">
              <CInputGroup className="mb-3">
                  <CInputGroupPrepend>
                    <CInputGroupText>
                    <CIcon name="cil-user" />
                    </CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    onChange={handleNoTelp}
                    value={noTelp}
                    type="number"
                    placeholder="No HP"
                    autoComplete="no_hp"
                  />
                </CInputGroup>
              <CInputGroup className="mb-3">
                  <CInputGroupPrepend>
                    <CInputGroupText>
                    <CIcon name="cil-user" />
                    </CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    onChange={handleNIP}
                    value={nip}
                    type="number"
                    placeholder="NIP"
                    autoComplete="nip"
                  />
                </CInputGroup>
                <CInputGroup className="mb-3">
                  <CInputGroupPrepend>
                    <CInputGroupText>@</CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    onChange={handleEmail}
                    value={email}
                    type="email"
                    placeholder="email"
                    autoComplete="email"
                  />
                </CInputGroup>

                <CInputGroup className="mb-3">
                  <CInputGroupPrepend>
                    <CInputGroupText>
                      <CIcon name="cil-lock-locked" />
                    </CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    onChange={handlePassword}
                    value={password}
                    type="password"
                    placeholder="Password"
                    autoComplete="new-password"
                  />
                </CInputGroup>
                <CInputGroup className="mb-4">
                  <CInputGroupPrepend>
                    <CInputGroupText>
                      <CIcon name="cil-lock-locked" />
                    </CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    onChange={handleVerifPassword}
                    value={verifPassword}
                    type="password"
                    placeholder="Repeat password"
                    autoComplete="new-password"
                  />
                </CInputGroup>
               <CFormGroup>
              <CLabel style={{ fontWeight: "bold" }}>GANTI FOTO</CLabel>
              <ImageUploading
          onChange={imageHandler}
      >
        {({ imageList, onImageUpload }) => (
          // write your building UI
          <div className="imageuploader">

            <div className="mainBtns">
            <button className="btn btn-primary mr-1" onClick={onImageUpload}>Upload Image</button>
            
            </div>
            {imageList.map((image) => (
              <div className="imagecontainer" key={image.key}>
                <img src={image.dataURL} />
              </div>
            ))}
          </div>
        )}
      </ImageUploading>
      {
        loadingImage === true &&
      <CSpinner style={{marginTop:15}} color="info" />
      }


              {/* <CInput
                style={{
                  height: "100%",
                  background: "transparent",
                  border: "none",
                }}
                onChange={imageHandler}
                type="file"
                id="input"
                name="image-upload"
                accept="image/*"
              /> */}
            </CFormGroup>

                <CRow>
                  <CCol xs="12" sm="6">
                    <CButton onClick={handleKembali} className="btn-facebook mb-1" block>
                      <span>Kembali</span>
                    </CButton>
                  </CCol>
                  <CCol xs="12" sm="6">
                    <CButton onClick={handleDaftar} className="btn-twitter mb-1" block>
                      <span>Daftar</span>
                    </CButton>
                  </CCol>
                </CRow>
                
              {
                loading === true &&
                <CSpinner style={{marginTop:'5%', marginLeft:'48%'}} color="info" />
              }
              </CCardFooter>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Register;
