import React,{useState} from "react";
import { Link,useHistory } from "react-router-dom";
import Backgorund from "../../../assets/bg-abstract-white.jpeg";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CSpinner
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import axios from 'axios'

const Login = () => {
  const history = useHistory();
  const [nik,setNIK] = useState("");
  const [password,setPassword] = useState("");
  const [loading,setLoading] = useState(false)

  React.useEffect(()=>{
    var user_nik = localStorage.getItem("nik");

    if (user_nik !== null) {
      history.push('/dashboard')
    } else {
      console.log("Mohon login terlebih dahulu")
    }
  },[])

  const handleNIK = e =>{
    setNIK(e.target.value)
  }

  const handlePassword = e =>{
    setPassword(e.target.value)
  }

  const handleButtonLogin = e =>{
    setLoading(true)
    if (nik === "") {
      setLoading(false)
      alert("NIK tidak boleh kosong")
    } else if (password === "") {
      setLoading(false)
      alert("Password tidak boleh kosong")
    } else  {
      axios.get(""+window.server+"rest-api-sip/profile/login/login.php?nik="+nik+"&password="+password+"")
        .then((res)=>{
            if (res.data.message === "No post found") {
              setLoading(false)
              alert("NIK atau Password salah")
            } else {
              setLoading(false)
              localStorage.setItem("nik",res.data[0].nik)
              localStorage.setItem("id",res.data[0].id)
              localStorage.setItem("nama_lengkap",res.data[0].nama_lengkap)
              localStorage.setItem("image",res.data[0].image)
              localStorage.setItem("no_telp",res.data[0].no_telp)
              // alert(JSON.stringify(res.data[0].nik))
              history.push("/skp")
            }
        })
        .catch((err)=>{
          console.log(err)
        })
    }
  }

  return (
    <div
      style={{ backgroundImage: `url(${Backgorund})` }}
      className="c-app c-default-layout flex-row align-items-center"
    >
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>PPNPN</h1>
                    <p className="text-muted">Masuk dengan NIK anda</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={nik}
                        type="number"
                        placeholder="NIK"
                        autoComplete="NIK"
                        onChange={handleNIK}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        value={password}
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        onChange={handlePassword}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol  xs="12">
                        <CButton onClick={handleButtonLogin} color="primary" className="px-4">
                          Masuk
                        </CButton>
                        {
                          loading === true &&
                          <CSpinner style={{marginLeft:'50%'}} color="info" />
                        }

                      </CCol>
                    </CRow>
                    
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard
                className="text-white bg-primary py-5 d-md-down-none"
                style={{ width: "44%" }}
              >
                <CCardBody className="text-center">
                  <div>
                    <h2>Pendaftaran</h2>
                    <p>
                      Bagi PPNPN yang belum mendaftar di sistem SIP, silahkan mendaftar terlebih dahulu.
                    </p>
                    <Link to="/register">
                      <CButton
                        color="primary"
                        className="mt-3"
                        active
                        tabIndex={-1}
                      >
                        Daftar Sekarang!
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
