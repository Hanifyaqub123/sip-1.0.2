import React,{useState,useEffect} from 'react'
import {
    CButtonToolbar,
    CForm,
    CCol,
    CContainer,
    CRow,
    CLabel,
    CInput,
    CFormGroup,
    CButton,
    CSpinner,
    CCard
  } from '@coreui/react'
  import axios from 'axios'
  import {  useHistory } from 'react-router-dom'



  const DaftarCuti = () =>{

    const history = useHistory();

    const [nik] = useState(localStorage.getItem('nik'))
    const [data,setData] = useState([])


    useEffect(()=>{
        handleGetData()
        handleDateData()
        handleGetProfiel()

    },[])

    const handleGetProfiel = () =>{
        axios.get("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/getjabatan.php?nik=" + nik + "")
            .then((res)=>{
                if (res.data.message === "No post found") {
                    console.log('data kosong')
                    // setNamaJabatan("-")
                    // setUnitKerja("-")
                  } else {
                    var data = res.data.pop();
                    localStorage.setItem('jabatan',data.nama_jabatan)
                    localStorage.setItem('unit_kerja',data.unit_kerja)
                  }
            })
            .catch((err)=>{
                console.log(err)
            })
    }

    const handleDateData = () =>{
        let newDate = new Date()
        let date = newDate.getDate();
        localStorage.setItem('date',date)
        // setTanggal(date)
        let month = newDate.getMonth() + 1;
        let year = newDate.getFullYear();
        // setTahun(year)
        localStorage.setItem('year',year)

        

        if (month === 1) {
            localStorage.setItem('month','Januari')
            // setBulan('Januari')
        } else  if (month === 2) {
            localStorage.setItem('month','Febuari')
        }else  if (month === 3) {
            localStorage.setItem('month','Maret')
        }else  if (month === 4) {
            localStorage.setItem('month','April')
        }else  if (month === 5) {
            localStorage.setItem('month','Mei')
        }else  if (month === 6) {
            localStorage.setItem('month','Juni')
        }else  if (month === 7) {
            localStorage.setItem('month','Juli')
        }else  if (month === 8) {
            localStorage.setItem('month','Agustus')
        }else  if (month === 9) {
            localStorage.setItem('month','September')
        }else  if (month === 10) {
            localStorage.setItem('month','Oktober')
        }else  if (month === 11){
            localStorage.setItem('month','November')
        }else  if (month === 12) {
            localStorage.setItem('month','Desember')
        }
    }

    const handleGetData = () =>{
        axios.get(""+window.server+"rest-api-sip/profile/cuti/getcuti.php?nik="+nik+"")
            .then((res)=>{
                if (res.data.message === "No post found") {
                    // alert(JSON.stringify(res.data))
                    setData([])
                } else {
                    // alert(JSON.stringify(res.data))
                    setData(res.data)
                }
              
            })
    }

      return(
          <CContainer>
              <CCard style={{padding: 20,}}>
             <label>*Untuk cetak cuti, silahkan pilih table yang tertera</label>
             { data.map((res,index)=>{
                 return(
                    <table  >
                    <td style={{width:'3%'}}>
                        <tr >{index+1}</tr>
                    </td>
                    <td style={{width:'25%'}}>
                    <tr style={{display:'flex',flexDirection:'column',}}>
                           <label style={{fontSize:20}}>{res.tanggal_awal} s.d. {res.tanggal_akhir}</label>
                           <label >{res.jenis_cuti}</label>
                      </tr>
                    </td>
                    <td style={{width:'20%'}}>
                    <tr style={{display:'flex',flexDirection:'column',alignItems:'center'}}>
                           <label style={{fontSize:20}}>{res.hari_cuti} Hari Kalender</label>
                      </tr>
                    </td>
                    <td style={{width:'10%'}}>
                    <tr style={{display:'flex',flexDirection:'column',textAlign:'center'}}>
                           <label style={{fontSize:20}}>{res.status}</label>
                           <label >{res.tanggal_status}</label>
                      </tr>
                    </td>
                    <td style={{width:'30%'}}>
                    <tr style={{display:'flex',flexDirection:'column',textAlign:'center'}}>
                           <label style={{fontSize:20}}>No Surat : {res.no_surat}</label>
                           <label >Tanggal Surat : {res.tanggal_pembuatan}</label>
                      </tr>
                    </td>
                    <td style={{width:'10%'}}>
                        <div style={{margin:10}}>
                        <CButton style={{marginBottom:10}} 
                        onClick={()=>{
                            if (window.confirm('Apakah anda yakin ?')) {

                                axios
                                .delete("" + window.server + "rest-api-sip/profile/file/deletefile.php", {
                                  data: { user_profile: res.image },
                                })
                                .then((res) => {
                                    console.log(res.data)
                    
                                //   alert(JSON.stringify(res.data))
                                //   window.location.reload();
                                })
                                .catch((err) => {
                                  console.log(err)
                                })
                    
                    
                              axios
                                .delete("" + window.server + "rest-api-sip/profile/cuti/deletecuti.php", {
                                  data: { id: res.id },
                                })
                                .then((res) => {
                                  alert(JSON.stringify(res.data))
                                  window.location.reload();
                                })
                                .catch((err) => {
                                  console.log(err)
                                })
                            }
                        }}  
                        color="danger">Hapus</CButton>
                    <CButton onClick={(e)=>{
                        window.open(""+window.server1+"printcuti","_blank")
                        localStorage.setItem('jenis_cuti',res.jenis_cuti)
                        localStorage.setItem('keterangan',res.keterangan)
                        localStorage.setItem('hari_cuti',res.hari_cuti)
                        localStorage.setItem('tanggal_awal',res.tanggal_awal)
                        localStorage.setItem('tanggal_akhir',res.tanggal_akhir)
                        localStorage.setItem('alamat_cuti',res.alamat_cuti)
                        localStorage.setItem('no_surat',res.no_surat)
                        localStorage.setItem('tanggal_pembuatan',res.tanggal_pembuatan)
                        }} color="info">Print</CButton>
                    <CButton style={{marginBottom:10,marginTop:10}} onClick={()=>{
                        if (res.image === "") {
                            alert("File tidak ada")
                        } else {
                            window.open(""+window.server+"rest-api-sip/profile/file/pdf/"+res.image+"")
                        }
                        }}  color="info">Dokumen</CButton>

                        </div>
                   
                    </td>
                  
                    
                </table>
                 )
             })
              
             
             }
                   </CCard>
          </CContainer>
      )
  }
  export default DaftarCuti