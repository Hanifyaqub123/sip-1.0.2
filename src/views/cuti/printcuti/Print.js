import React,{useEffect,useState} from 'react'

const Print = () =>{
    const [bulan] = useState(localStorage.getItem('month'))
    const [tanggal] = useState(localStorage.getItem('date'))
    const [tahun] = useState(localStorage.getItem('year'))

    const [nama] = useState(localStorage.getItem('nama_lengkap'))
    const [jabatan] = useState(localStorage.getItem('jabatan'))
    const [unitKerja] = useState(localStorage.getItem('unit_kerja'))

    const [jenisCuti] = useState(localStorage.getItem('jenis_cuti'))
    const [keteranganCuti] = useState(localStorage.getItem('keterangan'))

    const [hariCuti] = useState(localStorage.getItem('hari_cuti'))
    const [tanggalAwal] = useState(localStorage.getItem('tanggal_awal'))
    const [tanggalAkhir] = useState(localStorage.getItem('tanggal_akhir'))
    const [noTelp] = useState(localStorage.getItem('no_telp'))
    const [alamatCuti] = useState(localStorage.getItem('alamat_cuti'))

    const [noSurat] = useState(localStorage.getItem('no_surat'))
    const [tanggalSurat] = useState(localStorage.getItem('tanggal_pembuatan'))

    useEffect(()=>{
        window.print();
        
    },[])
    return(
        <div class="book">
        <div class="page">
                <table style={{marginTop:10}}>
                    <tr>
                        <th style={{ fontFamily:'MyFont',fontSize:15,width:'50%',borderWidth:0}}></th>
                        <th style={{fontFamily:'MyFont',fontSize:17,width:'50%',borderWidth:0,fontWeight:'normal',paddingLeft:'9%'}}>Salatiga, {tanggal} {bulan} {tahun}</th>
                    </tr>
                    <tr>
                        <th style={{ fontFamily:'MyFont',fontSize:15,width:'50%',borderWidth:0}}></th>
                        <th style={{fontFamily:'MyFont',fontSize:17,width:'50%',borderWidth:0,fontWeight:'normal',paddingLeft:'9%'}}>Kepada</th>
                    </tr>
                    <tr>
                        <th style={{ fontFamily:'MyFont',fontSize:15,width:'50%',borderWidth:0}}></th>
                        <th style={{fontFamily:'MyFont',fontSize:17,width:'50%',borderWidth:0,fontWeight:'normal',paddingLeft:'9%'}}>Yth. Kepala Kantor Pertanahan Kota Salatiga</th>
                    </tr>
                    <tr>
                        <th style={{ fontFamily:'MyFont',fontSize:15,width:'50%',borderWidth:0}}></th>
                        <th style={{fontFamily:'MyFont',fontSize:17,width:'50%',paddingLeft:'12%',borderWidth:0,fontWeight:'normal'}}>di</th>
                    </tr>
                    <tr>
                        <th style={{ fontFamily:'MyFont',fontSize:15,width:'50%',borderWidth:0}}></th>
                        <th style={{fontFamily:'MyFont',fontSize:17,width:'50%',paddingLeft:'14%',borderWidth:0,fontWeight:'normal'}}>Salatiga</th>
                    </tr>
                    
                </table>

                <table style={{marginTop:'1%'}}>
                <tr style={{width:'100%'}}>
                    <th colSpan='2' style={{textAlign:'center',fontFamily:'MyFont',fontSize:17,fontWeight:'normal',borderWidth:0,}}>FORMULIR PERMINTAAN DAN PEMBERIAN CUTI</th>
                    </tr>
                </table>

                <table style={{width:'95%',margin:'2%',marginTop:'1%'}}>
                    <tr>
                        <th colSpan='4' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'3%'}}>I.    DATA PEGAWAI</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'15%'}}>Nama </th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%'}}>{nama}</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'15%'}}>NIP </th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'30%'}}>-</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'15%'}}>Jabatan </th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%'}}>{jabatan}</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'15%'}}>Masa Kerja </th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'30%'}}>-</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'15%'}}>Unit Kerja</th>
                        <th colSpan='3' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%'}}>Kantor Pertanahan Kota Salatiga – {unitKerja}</th>
                       
                    </tr>
                </table>
               
                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='4' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'3%'}}>II.	JENIS CUTI YANG DIAMBIL**</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>1.   Cuti Tahunan </th>
                        {
                            jenisCuti === "Cuti Tahunan" ?
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}>√</th>
                            :
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}></th>


                        }
                        
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>2.	Cuti Besar </th>
                        {
                            jenisCuti === "Cuti Besar" ?
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}>√</th>
                            :
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}></th>


                        }
                    </tr>

                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>3.	Cuti Sakit </th>
                        {
                            jenisCuti === "Cuti Sakit" ?
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}>√</th>
                            :
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}></th>


                        }
                        
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>4.	Cuti Melahirkan </th>
                        {
                            jenisCuti === "Cuti Melahirkan" ?
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}>√</th>
                            :
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}></th>


                        }
                    </tr>

                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>5.	Cuti Karena Alasan Penting </th>
                        {
                            jenisCuti === "Cuti Karena Alasan Penting" ?
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}>√</th>
                            :
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}></th>


                        }
                        
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>6.	Cuti di Luar Tanggungan Negara </th>
                        {
                            jenisCuti === "di Luar Tanggungan Negara" ?
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}>√</th>
                            :
                            <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',width:'10%',textAlign:'center'}}></th>


                        }
                        
                    </tr>
                   
                </table>
        
                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='4' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'3%'}}>III.	ALASAN CUTI</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>{keteranganCuti}</th>
                    </tr>

                 
                </table>
        
                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='6' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'3%'}}>IV.	LAMANYA CUTI</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',textAlign:'center',width:'5%'}}>Selama </th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',textAlign:'center',width:'15%'}}>{hariCuti} Hari Kalender</th>
                        
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',textAlign:'center',width:'10%'}}>Mulai Tanggal</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',textAlign:'center',width:'15%'}}>{tanggalAwal}</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',textAlign:'center',width:'3%'}}>s.d</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',textAlign:'center',width:'15%'}}>{tanggalAkhir}</th>
                    </tr>
                   
                </table>

                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='5' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'3%'}}>V.	CATATAN CUTI***</th>
                    </tr>
                    <tr>
                        <th colSpan='3' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%'}}>1.	Cuti Tahunan</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'25%'}}>2.	Cuti Besar</th>
                        <th  style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'25%'}}></th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}>Tahun</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'7%'}}>Sisa</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}>Keterangan</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'25%'}}>3.	Cuti Sakit</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'15%'}}></th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}>N-2</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'7%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'25%'}}>4.	Cuti Melahirkan</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'15%'}}></th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}>N-1</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'7%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'25%'}}>5.    Cuti Karena Alasan Penting</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'15%'}}></th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}>N</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'7%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'8%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'30%'}}>6.	Cuti Di Luar Tanggungan Negara</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'15%'}}></th>
                    </tr>
                </table>

                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='5' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%'}}>VI.	ALAMAT SELAMA MENJALANKAN CUTI</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'10%'}}>Telp</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'40%'}}>{noTelp}</th>
                    
                    </tr>

                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%'}}>{alamatCuti}</th>
                        <th colSpan='2' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%',textAlign:'center'}}>
                            <div>Hormat Saya</div>
                            <div style={{paddingTop:20}}>{nama}</div>
                            <div>NIP -</div>

                        </th>
                    
                    </tr>
                </table>

                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='4' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%'}}>VII.	PERTIMBANGAN ATASAN LANGSUNG**</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}>DISETUJUI</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}>PERUBAHAN****</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}>DITANGGUHKAN****</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'100%'}}>TIDAK DISETUJUI</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%',height:15}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'100%'}}></th>
                    </tr>

                    <tr>
                        <th colSpan='3' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderBottomWidth:0,borderLeftWidth:0}}></th>
                        <th  style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%',textAlign:'center'}}>
                            <div>Kepala Sub Bagian Tata Usaha,</div>
                            <div style={{paddingTop:30}}>Hartyas Kusumastuti, S.E., M.AP., M.PP.</div>
                            <div>NIP 198008152003122003</div>

                        </th>
                    
                    </tr>
                </table>

                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='4' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%'}}>
                            <div>VIII.	KEPUTUSAN PEJABAT YANG BERWENANG MEMBERIKAN CUTI</div>
                            <div style={{paddingLeft:'4%'}}>Nomor   : {noSurat}</div>
                            <div style={{paddingLeft:'4%'}}>Tanggal : {tanggalSurat}</div>
                        </th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}>DISETUJUI</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}>PERUBAHAN****</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}>DITANGGUHKAN****</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'100%'}}>TIDAK DISETUJUI</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%',height:15}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'20%'}}></th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'1%',width:'100%'}}></th>
                    </tr>

                    <tr>
                        <th colSpan='3' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderBottomWidth:0,borderLeftWidth:0}}></th>
                        <th  style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',width:'50%',textAlign:'center'}}>
                            <div>Kepala Kantor Pertanahan Kota Salatiga,</div>
                            <div style={{paddingTop:30}}>Mulyanto,S.SiT</div>
                            <div>NIP 1966903031991031002</div>

                        </th>
                    
                    </tr>
                </table>

                <table style={{width:'95%',margin:'2%'}}>
                    <tr>
                        <th colSpan='2' style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>Catatan :</th>
                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>*</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>Coret yang tidak perlu</th>

                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>**</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>Pilih salah satu dengan memberi tanda centang ( √ )</th>

                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>***</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>Di isi oleh pejabat yang menangani bidang kepegawaian sebelum PNS mengajukan cuti</th>

                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>****</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>Diberi tanda centang dan alasannya</th>

                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>N</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>= Cuti tahun berjalan</th>

                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>N1</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>= Sisa cuti 1 tahun sebelumnya</th>

                    </tr>
                    <tr>
                        <th style={{fontSize:13,fontFamily:'MyFont',paddingLeft:'2%',width:'10%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>N2</th>
                        <th style={{fontSize:13,fontFamily:'MyFont',fontWeight:'normal',paddingLeft:'2%',borderTopWidth:0,borderLeftWidth:0,borderRightWidth:0,borderBottomWidth:0}}>= Sisa cuti 2 tahun sebelumnya</th>

                    </tr>
                    
                    
                </table>
        
        </div>
    </div>
    )
}


export default Print;