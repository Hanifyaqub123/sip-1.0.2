import React,{useState,useEffect} from 'react'
import {
    CButtonToolbar,
    CForm,
    CCol,
    CContainer,
    CRow,
    CInput,
    CButton,
    CCard
  } from '@coreui/react'
  import axios from 'axios'
import {Image} from 'cloudinary-react'
import {  useHistory } from 'react-router-dom'

import { Viewer } from '@react-pdf-viewer/core';
// import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import {defaultLayoutPlugin} from '@react-pdf-viewer/default-layout'
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';

import { Worker } from '@react-pdf-viewer/core';



  const BuatCuti = () =>{
    const history = useHistory();

    const defaultLayoutPluginInstance = defaultLayoutPlugin();

    const [pdfView,setPdfview] = useState(false)

    const [percentageUpload,setPercentageUpload] = useState(0)
    const [ButtonUploadView,setButtonView] = useState(false)
    const [uploadProgressView,setUploadProgressView] = useState()

    var today = new Date();

    const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    const options = [{id:1,nm:"Baru",title:"Baru"},{id:2,nm:"Perpanjang",title:"Perpanjang"}];


    const [nik] = useState(localStorage.getItem('nik'))
    const [nama,setNama] = useState('')
    const [unitKerja,setUnitKerja] = useState('')

    const[jenisCuti,setJenisCuti] = useState('')
    const [pengajuan,SetPengajuan] = useState('')
    const [tanggalCutiAwal,setTanggalCutiAwal] = useState('')
    const [tanggalCutiAkhir,setTanggalCutiAkhir] = useState('')
    const [jumlahHariCuti,setjumlahhariCuti] = useState('')
    const [keterangan,setKeterangan] = useState('')
    const [alamatCuti,setAlamatCuti] = useState('')

    const [hariAwal,setHariAwal] = useState('')
    const [bulanAwal,setBulanAwal] = useState('')
    const [tahunAwal,setTahunAwal] = useState('')

    const [hariAkhir,setHariAkhir] = useState('')
    const [bulanAkhir,setBulanAkhir] = useState('')
    const [tahunAkhir,setTahunAkhir] = useState('')

    const [imageSelected,setImageSelected] = useState("")
    // Bypass CORS https://cors-anywhere.herokuapp.com/{Link URLnya}
    const [urlImage,setUrlImage] = useState("")
    

    useEffect(()=>{
        handleGetProfile();
        handleGetUnitKerja();
        // handleJumlahCuti();
    },[])

    const uploadImage = () =>{
        setButtonView(true)
        setUploadProgressView(true)

            const options =  {
                onUploadProgress : (progressEvent) =>{
                    const {loaded,total} = progressEvent;
                    let percent = Math.floor(loaded * 100 / total) 
                    // console.log(` ${loaded}kb of ${total}kb | ${percent}% `);
                    setPercentageUpload(percent)
                    if (percent === 100) {
                        setUploadProgressView(false)
                        setPdfview(true)
                    }
                }
            }
    
    
            const formData  = new FormData()
              formData.append("user_image",imageSelected)
              formData.append("user_name",nik)

              axios.post(""+window.server+"rest-api-sip/profile/file/uploaduserpdf.php",formData,options)
              .then((res)=>{
                // alert(JSON.stringify(res.data))
                setUrlImage(res.data.url_image)
              })
        
        //   axios.post("https://api.cloudinary.com/v1_1/drznaeddx/image/upload",formData,options)
        //     .then((res)=>{
        //       // alert(JSON.stringify(res.data.secure_url))
        //       setUrlImage(res.data.secure_url)
        //     })

          

        // console.log(imageSelected.name)
      }

    const handleGetProfile = () =>{
        axios.get(""+window.server+"rest-api-sip/profile/user/identitas/getprofile.php?nik="+nik+"")
            .then((res)=>{
                res.data.map((res)=>{
                        setNama(res.nama_lengkap)
                })
            })
            .catch((err)=>{
                console.log(err)
            })
    }

    const handleGetUnitKerja = () =>{

        axios.get("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/getjabatan.php?nik=" + nik + "")
        .then((res) => {
        // alert(JSON.stringify(res.data.pop()))
        if (res.data.message === "No post found") {
            setUnitKerja("-")
        } else {
            var data = res.data.pop();
            setUnitKerja(data.unit_kerja)
        }
        })
        .catch((err) => {
        console.log(err)
        })
    }

    const handlePengajuan = e =>{
        SetPengajuan(e.target.value)
    }

    const handleSelectCuti = e =>{
        setJenisCuti(e.target.value)
       
    }

    const handleCutiAwal = e =>{
        setTanggalCutiAwal(e.target.value)
        const day = e.target.value.split('-').pop();
        setHariAwal(day)
        const month = e.target.value.substring(e.target.value.indexOf('-') +1);
        const month1 = month.substring(0,2);
        const years = e.target.value.split('-', 1);
        setTahunAwal(years)
        
        if (month1 === '01' ) {
            setBulanAwal("Januari")
        } else  if (month1 === '02' ) {
            setBulanAwal("Febuari")
        }else  if (month1 === '03' ) {
            setBulanAwal("Maret")
        }else  if (month1 === '04' ) {
            setBulanAwal("April")
        }else  if (month1 === '05' ) {
            setBulanAwal("Mei")
        }else  if (month1 === '06' ) {
            setBulanAwal("Juni")
        }else  if (month1 === '07' ) {
            setBulanAwal("Juli")
        }else  if (month1 === '08' ) {
            setBulanAwal("Agustus")
        }else  if (month1 === '09' ) {
            setBulanAwal("September")
        }else  if (month1 === '10' ) {
            setBulanAwal("Oktober")
        }else  if (month1 === '11' ) {
            setBulanAwal("November")
        }else  if (month1 === '12' ) {
            setBulanAwal("Desember")
        }

    }

    const handleCutiAkhir = e =>{
        setTanggalCutiAkhir(e.target.value)
        var onDay = 24 * 60 * 60 * 1000;

        var firstDate = new Date(tanggalCutiAwal.replace(/-/g, ','));
        var secondDate = new Date(e.target.value.replace(/-/g, ','));

        var diffDays = Math.round(Math.abs((firstDate-secondDate)/onDay))

        setjumlahhariCuti(diffDays+1)

        const day = e.target.value.split('-').pop();
        setHariAkhir(day)
        const month = e.target.value.substring(e.target.value.indexOf('-') +1);
        const month1 = month.substring(0,2);
        const years = e.target.value.split('-', 1);
        setTahunAkhir(years)
        
        if (month1 === '01' ) {
            setBulanAkhir("Januari")
        } else  if (month1 === '02' ) {
            setBulanAkhir("Febuari")
        }else  if (month1 === '03' ) {
            setBulanAkhir("Maret")
        }else  if (month1 === '04' ) {
            setBulanAkhir("April")
        }else  if (month1 === '05' ) {
            setBulanAkhir("Mei")
        }else  if (month1 === '06' ) {
            setBulanAkhir("Juni")
        }else  if (month1 === '07' ) {
            setBulanAkhir("Juli")
        }else  if (month1 === '08' ) {
            setBulanAkhir("Agustus")
        }else  if (month1 === '09' ) {
            setBulanAkhir("September")
        }else  if (month1 === '10' ) {
            setBulanAkhir("Oktober")
        }else  if (month1 === '11' ) {
            setBulanAkhir("November")
        }else  if (month1 === '12' ) {
            setBulanAkhir("Desember")
        }

    }

    const handleKeterangan = e =>{
        setKeterangan(e.target.value)
    }

    const handleAlamatCuti = e =>{
        setAlamatCuti(e.target.value)
    }
    
    const handleSimpan = () =>{
       
           var tanggalAwal = hariAwal+' '+bulanAwal+' '+tahunAwal
           var tanggalAkhir = hariAkhir+' '+bulanAkhir+' '+tahunAkhir

        if (jenisCuti === "") {
            alert("Jenis cuti tidak boleh kosong")
        } else if (pengajuan === "") {
            alert("Pengajuan tidak boleh kosong")
        }else if (tanggalCutiAwal === "") {
            alert("Tanggal awal tidak boleh kosong")
        }else if (tanggalCutiAkhir === "") {
            alert("Tanggal akhir tidak boleh kosong")
        }else if (pengajuan === "") {
            alert("Pengajuan tidak boleh kosong")
        }else if (keterangan === "") {
            alert("Keterangan tidak boleh kosong")
        }else if (alamatCuti === "") {
            alert("Keterangan dokter tidak boleh kosong")
        }else if (keterangan === "") {
            alert("Keterangan tidak boleh kosong")
        }else if (alamatCuti === "") {
            alert("Keterangan Dokter tidak boleh kosong")
        }else {

            const post = {
                nik : nik,
                tanggal_awal:tanggalAwal,
                tanggal_akhir:tanggalAkhir,
                jenis_pengajuan:pengajuan,
                jenis_cuti:jenisCuti,
                hari_cuti:jumlahHariCuti,
                status:'Pending',
                tanggal_status: date,
                no_surat : '-',
                tanggal_pembuatan: "-",
                image:urlImage,
                keterangan:keterangan,
                alamat_cuti:alamatCuti
           }
           axios.post(""+window.server+"rest-api-sip/profile/cuti/insertcuti.php",post)
            .then((res)=>{
                alert(JSON.stringify(res.data))
                history.push('/daftarcuti')
            })
            .catch((err)=>{
                console.log(err)
            })
        }
  

    }

      return(
          <CContainer>
              <CCard style={{padding:20}}>
               <CRow>
                <CCol md="6" className="py-3">
                    <h5>Form Cuti</h5>
                    <div style={{marginTop:20,display:'flex',flexDirection:'row',borderBottomWidth:1,paddingBottom:10}}>
                        <label style={{fontSize:15,width:100,paddingTop:5}}>Jenis Cuti:</label>
                        <select onChange={handleSelectCuti} class="form-control">
                       <option value="">--Pilih--</option>
                       <option value="Cuti Sakit">Cuti Sakit</option>
                       <option value="Cuti Melahirkan">Cuti Melahirkan</option>
                       <option value="Cuti Alasan Penting">Cuti Alasan Penting</option>
                   </select>
                    </div>
                    <div style={{marginTop:20,display:'flex',flexDirection:'row'}}>
                        <label style={{fontSize:15,width:100,paddingTop:5}}>Pengajuan:</label>
                        <div style={{display:'flex',flexDirection:'row'}}>
                            {
                                options.map((item,i)=>{
                                    return(
                                        <div style={{display:'flex',flexDirection:'row',alignItems:'center'}}>
                                            <input name={item.nm} checked={pengajuan === item.nm}  value={item.nm} type="radio" onChange={handlePengajuan}/>
                                            <label style={{paddingTop:5,paddingLeft:5,paddingRight:10}}>{item.title}</label>
                                        </div>
                                    )
                                })
                            }
                            
                        </div>
                    </div>

                    <div style={{marginTop:20,display:'flex',flexDirection:'row'}}>
                        <label style={{fontSize:15,width:250}}>Tgl Cuti:</label>
                    <CInput
                        type="date"
                        value={tanggalCutiAwal}
                        onChange={handleCutiAwal}
                      />
                      <label style={{padding: 5,}}>s/d</label>
                      <CInput
                        type="date"
                        value={tanggalCutiAkhir}
                        onChange={handleCutiAkhir}
                      />
                    </div>
                    <div style={{marginTop:20,display:'flex',flexDirection:'row'}}>
                    <label style={{fontSize:15,width:100,paddingTop:5}}>Lama Cuti:</label>
                    <label style={{padding: 5,}}>{jumlahHariCuti}  Hari Kalender</label>
                    </div>

                    <div style={{marginTop:20,display:'flex',flexDirection:'row'}}>
                    <label style={{fontSize:15,width:100,paddingTop:5}}>Keterangan:</label>
                   <textarea
                   value={keterangan}
                   onChange={handleKeterangan}
                   style={{width:'80%'}}/>
                    </div>

                    <div style={{marginTop:20,display:'flex',flexDirection:'row'}}>
                    <label style={{fontSize:15,width:100,paddingTop:5}}>Alamat Cuti:</label>
                   <textarea 
                   style={{width:'80%'}}
                   value={alamatCuti}
                   onChange={handleAlamatCuti}
                   />
                    </div>
                    
                </CCol>
                <CCol md="6" className="py-3">
                    <h5>Keterangan Pegawai</h5>
                    <div style={{marginTop:20,display:'flex',flexDirection:'row',borderBottomWidth:1}}>
                        <label style={{fontSize:15,width:100}}>Nama</label>
                        <label style={{fontSize:15,width:'100%'}}>: {nama}</label>
                    </div>
                    <div style={{display:'flex',flexDirection:'row',borderBottomWidth:1}}>
                        <label style={{fontSize:15,width:100,paddingTop:5}}>NIK</label>
                        <label style={{fontSize:15,width:'100%',paddingTop:5}}>: {nik}</label>
                    </div>
                    <div style={{display:'flex',flexDirection:'row',borderBottomWidth:1,paddingBottom:10}}>
                        <label style={{fontSize:15,width:100,paddingTop:5}}>Unit Kerja</label>
                        <label style={{fontSize:15,width:'100%',paddingTop:5}}>: {unitKerja}</label>
                    </div>

                    <h5>Informasi kelengkapan syarat cuti</h5>
                    <label>Format penamaan file : cutisakit_20/12/2021_Fajar Eka.pdf</label>
                    <div style={{paddingTop:'5%'}}>
                        <input 
                        type="file"
                        onChange={(e)=>{
                            const { target } = e
                            if(target.value.length > 0){
                            var filename = e.target.files[0].name
                            var fileExtension = filename.split('.').pop();
                            if (fileExtension === "pdf") {
                                setImageSelected(e.target.files[0]);
                            }else{
                                alert("File bukan PDF")
                                setImageSelected("");
                            }
                            } else {
                                alert("File tidak boleh kosong, pilih file kembali")
                                setImageSelected("");
                            }
                           
                        }}
                        />
                       { 
                       ButtonUploadView === false &&
                       <button 
                        style={{marginRight:'5%'}}
                        onClick={uploadImage}
                        >Upload PDF</button>}
                        {
                            percentageUpload === 100 &&
                            <button
                            style={{marginRight:'5%'}}
                            onClick={uploadImage}
                            >Upload PDF</button>
                        }

                        {
                            uploadProgressView === true &&
                            <div style={{paddingTop:10}}>Uploading {percentageUpload}%</div>
                        }

                          {
                            uploadProgressView === false &&
                            <div style={{paddingTop:10}}></div>
                        }
                        {
                        pdfView === true &&
                            <div>
                            <a target="_blank" href={""+window.server+"rest-api-sip/profile/file/pdf/"+urlImage}>Cek Data</a>

                            </div>

                        }
                       {/* { 
                       pdfView === true &&
                       <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
                            <div  style={{
                                border: '1px solid rgba(0, 0, 0, 0.3)',
                                height: '750px',
                                marginTop:10
                            }}>

                                <Viewer
                            fileUrl={"https://immense-woodland-59629.herokuapp.com/"+window.server+"rest-api-sip/profile/file/pdf/"+urlImage}
                            plugins={[
                                // Register plugins
                                defaultLayoutPluginInstance,
                            ]}
                        />
                            </div>
                        
                        </Worker>} */}
                        
                    </div>   
                    <CButtonToolbar style={{paddingTop:20,paddingBottom:20}} justify="end">
        <CButton onClick={handleSimpan} style={{marginRight:10}} color="success">Simpan</CButton>
        <CButton style={{marginRight:10}} color="danger">Batal</CButton>
    </CButtonToolbar>
                </CCol>
          </CRow>
          </CCard>
          </CContainer>
      )
  }

  export default BuatCuti