import React,{useState} from 'react'
import { useHistory } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CSpinner
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios'


const Admin = () => {
  const history = useHistory();

  const [loading,setLoading] = useState(true)

  const [userLogin,setUserLogin] = useState('');
  const [userPassword,setPassword] = useState('');

  const handleLogin = e =>{
    setUserLogin(e.target.value)
  }

  const handlePassword = e =>{
    setPassword(e.target.value)
  }

  const handleButtonLogin = e =>{
   
    if (userLogin === '') {
      alert('NIP tidak boleh kosong')
    }else if (userPassword ==='') {
      alert('Password tidak boleh kosong')
    }else{
     setLoading(false)
     axios.get(''+window.server+'rest-api-sip/profile/admin/login/login.php?nip='+userLogin+'&password='+userPassword+'')
      .then(res=>{
        // alert(JSON.stringify(res.data.message))
        if (res.data.message === "No post found") {
            setLoading(true)
            alert('NIP atau Kata Sandi Salah')
            history.push('/admin')
        }else{
          localStorage.setItem("id", JSON.stringify(res.data[0].id))
          localStorage.setItem("nip",JSON.stringify(res.data[0].nip))
          localStorage.setItem("nama",JSON.stringify(res.data[0].nama))
          setLoading(true)
          // history.push('/dashboard')
        }
    
        // localStorage.setItem("user",JSON.stringify(res.data))

      })
     

      // history.push('/dashboard')
    }
  }

  return (
    <div  className="c-app c-default-layout flex-row align-items-center bgimg">
      { loading === false &&
       <div style={{position:'absolute',bottom:'10%',left:'50%',}}>
       <CSpinner
         color="primary"
         style={{width:'4rem', height:'4rem'}}
       />
 
     </div>
      }
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login Admin</h1>
                    <p className="text-muted">Login dengan NIP</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput 
                      type="text" 
                      placeholder="NIP" 
                      autoComplete="NIP"
                      value={userLogin}
                      onChange={handleLogin}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput 
                      type="password" 
                      placeholder="Password" 
                      autoComplete="current-password" 
                      value={userPassword}
                      onChange={handlePassword}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton onClick={handleButtonLogin} color="primary" className="px-4">Login</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Admin
