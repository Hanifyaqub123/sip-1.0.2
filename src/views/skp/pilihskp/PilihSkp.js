import React, { useState, useEffect } from "react";
import {
    CButton,
    CContainer,
    CRow,
    CCol,
    CCard,
    CButtonToolbar,
    CInput,
    CSwitch
} from "@coreui/react";
import { useHistory } from "react-router-dom";
import axios from "axios"


const PilihSkp = () => {
    const [showSkpAdd,setShowSkpAdd] = useState(false)
    const [id,setId] = useState("")
    const [nik] = useState(localStorage.getItem('nik'))
    const [namaKegiatan,setNamaKegaiatan] = useState("");
    const [satuan,setSatuan] = useState("");
    const history = useHistory();

    const [dataSKP,setDataSKP] = useState([]);

    const [ceklistSKP, setCeklistSKP] = useState([]);


    useEffect(()=>{
        handleGetDaftarSKP()
    },[])


    
    const handleGetDaftarSKP = () =>{
        axios.get(""+window.server+"rest-api-sip/profile/skp/buatskp/getdaftarskp.php?nik="+nik+"")
            .then((res)=>{
                if (res.data.message === "No post found") {
                   setDataSKP([])
                   alert("Data Kosong")
                } else {
                    setDataSKP(res.data)
                    
                    
                }
            })
    }

    const handleNamaKegiatan = e =>{
        setNamaKegaiatan(e.target.value)
    }

    const handleSatuan = e =>{
       setSatuan(e.target.value)
    }

    const handleDeleteDaftarSKP = e =>{
        if (window.confirm('Apakah anda yakin untuk menghapus ?')) {
            axios.delete(""+window.server+"rest-api-sip/profile/skp/buatskp/deletedaftarskp.php",{
                data: { id: e.target.value },
              })
                .then((res)=>{
                    alert(JSON.stringify(res.data))
                    window.location.reload();
                })
                .catch((err)=>{
                    console.log(err)
                })
          }

        

    }

    const handlePilihSKP = () =>{

        if (JSON.stringify(ceklistSKP) === "[]") {
            alert("Pilihan tidak boleh kosong")
        } else {
            axios.get(""+window.server+"rest-api-sip/profile/skp/sasaranskp/getsasaranskp.php?nik="+nik+"")
            .then((res)=>{
                if (res.data.message === "No post found") {
                    const post = {
                        nik : nik,
                        kegiatan_tugas : JSON.stringify(ceklistSKP)
                  }
            
                  axios.post(""+window.server+"rest-api-sip/profile/skp/sasaranskp/insertsasaranskp.php",post)
                    .then((res)=>{
                        alert(JSON.stringify(res.data))
                        history.push('/buatskp')
                    })

                } else {
                
                    if (res.data[0].nik === nik) {
                        var post = {
                            id : res.data[0].id,
                            nik: nik,
                            kegiatan_tugas : JSON.stringify(ceklistSKP)
                          }
                          axios.put(""+window.server+"rest-api-sip/profile/skp/sasaranskp/updatesasarnskp.php",post)
                            .then((res)=>{
                              alert(JSON.stringify(res.data))
                              history.push('/buatskp')
                            })
                            .catch((err)=>{
                              console.log(err)
                            })
                    }
                   
                }
            })
        }
       
   
    }



    const handleSimpanDaftarSKP = () =>{
        const post = {
            nik : nik,
            nama_kegiatan : namaKegiatan,
            satuan : satuan
        }

        axios.post(""+window.server+"rest-api-sip/profile/skp/buatskp/insertdaftarskp.php",post)
            .then((res)=>{
                alert(JSON.stringify(res.data))
                window.location.reload();
            })
            .catch((err)=>{
                console.log(err)
            })
    }
    
    return (

        <CContainer>
            <CCard style={{ padding: 20, }}>
                <h3>Daftar SKP</h3>
                <table>
                    <tr>
                        <th style={{ textAlign: 'center' }}>No</th>
                        <th style={{ textAlign: 'center' }}>Kegiatan</th>
                        <th style={{ textAlign: 'center' }}>Satuan Hasil</th>
                        <th colSpan="2" style={{ textAlign: 'center' }}>Pilih</th>

                    </tr>
                    {
                        dataSKP.map((res,index)=>{
                            return(
                            <tr>
                                <th style={{ textAlign: 'center'}} >{index+1}</th>
                                <th style={{ paddingLeft: 20 }} >{res.nama_kegiatan}</th>
                                <th style={{ textAlign: 'center' }}>{res.satuan}</th>
                                <th style={{ textAlign: 'center' }}>   
                                <input 
                                type="checkbox"  
                                onChange={(e)=>{
                                    if (e.target.checked) {
                                        setCeklistSKP([
                                            ...ceklistSKP,
                                            {
                                                nama_kegiatan : res.nama_kegiatan
                                            },
                                        ])
                                       
                                    } else {
                                        setCeklistSKP(
                                            ceklistSKP.filter((data)=> data.nama_kegiatan !== res.nama_kegiatan)
                                        )
                                }
                                   
                                }}
                                name={res.nama_kegiatan}
                                value={ceklistSKP}
                                />
                                {/* <CButton style={{ marginLeft: 10 }} color="danger">Hapus</CButton> */}
                                </th>
                                <th style={{ textAlign: 'center',width:'10%' }}>   
                                <CButton value={res.id} onClick={handleDeleteDaftarSKP}  color="danger">Hapus</CButton>
                                </th>
                               
                            </tr>
        
                            )
                        })
                    }
                  
                </table>
                <CButtonToolbar style={{ paddingTop: 20, paddingBottom: 20 }} justify="end">
                    <CButton onClick={handlePilihSKP} style={{ marginRight: 10 }} color="success">Pilih</CButton>
                    <CButton onClick={()=>{history.push("/buatskp")}} style={{ marginRight: 10 }} color="danger">Batal</CButton>
                    <CButton onClick={()=>{setShowSkpAdd(true)}} color="info">Tambah Baru</CButton>

                </CButtonToolbar>
            </CCard>
            {
                showSkpAdd === true &&
  <CCard style={{ padding: 20, }}>
  <h3>Tambah Baru Data Sasaran Kerja Pegawai (SKP)</h3>
  <CRow>
      <CCol lg="4" className="py-3">
          <div style={{ padding: 10, }}>
              <label style={{ fontSize: 15 }}>Nama Kegiatan Tugas Jabatan :	</label>
          </div>
          <div style={{ padding: 10, }}>
              <label style={{ fontSize: 15 }}>Satuan :	</label>
          </div>
      </CCol>
      <CCol lg="8" className="py-3">
          <div style={{ padding: 10, paddingTop: 5 }}>
              <CInput
                  value={namaKegiatan}
                  type="text"
                  onChange={handleNamaKegiatan}
              />
          </div>
          <div style={{ padding: 10, paddingTop: 5 }}>
              <CInput
                  value={satuan}
                  style={{ width: '40%' }}
                  type="text"
                  onChange={handleSatuan}
              />
          </div>


      </CCol>
  </CRow>
  <CButtonToolbar style={{ paddingTop: 20, paddingBottom: 20 }} justify="end">
                    <CButton onClick={handleSimpanDaftarSKP} style={{ marginRight: 10 }} color="success">Simpan </CButton>
                    <CButton onClick={()=>{setShowSkpAdd(false)}} style={{ marginRight: 10 }} color="danger">Batal</CButton>

                </CButtonToolbar>
</CCard>

            }
          
        </CContainer>
    )
}

export default PilihSkp