import React,{useState,useEffect} from 'react'
import {
    CDataTable,
    CBadge,
    CCollapse,
    CCardBody,
    CSpinner,
    CButton,
    CInput,
    CContainer,
    CRow,
    CCol,
    CCard,
    CSelect
  } from "@coreui/react";
  import {  useHistory } from 'react-router-dom'
  import axios from 'axios'


const SkpTahunanList = () =>{
    const [nik] = useState(localStorage.getItem('nik'))
    const year = (new Date()).getFullYear();
    const years = Array.from(new Array(10),( val, index) => index + year);
    const [data,setData] = useState([])

    const history = useHistory();

 
    const handleTahun = e =>{
        if (e.target.value === "") {
            alert("Tahun tidak boleh kosong")
        } else {
            axios.get("https://localhost/rest-api-sip/profile/skp/uploadskptahunan/getfile.php?user_name="+nik+"&tahun="+e.target.value+"")
            .then((res)=>{
                if (res.data.message === "No post found") {
                    alert("Data tidak ada")
                    setData([])
                } else {
                    setData(res.data)
                }
                
            })
        }
      
    }
   
    const handleDelete = (e) => {
        // alert(e.target.value)
        if (window.confirm('Apakah anda yakin ?')) {
          axios
          .delete(""+window.server+"rest-api-sip/profile/skp/uploadskptahunan/deletefile.php", {
            data: { id: e.target.value },
          })
          .then((res) => {
          
          alert(JSON.stringify(res.data))
          window.location.reload();
          });
      }
       
      };
  
    return(
        <CContainer>
      <CCard>
      
        <CRow style={{ padding: 20, }}>

          <CCol lg="12" className=" py-3">
            <h3 style={{ paddingBottom: 10 }}>Daftar SKP Tahunan</h3>
            <CSelect onChange={handleTahun} style={{width:'20%',marginRight:20,marginBottom:20}} custom name="ccmonth" id="ccmonth">
            <option value="">-- Pilih Tahun SKP --</option>
            {
                          years.map((year, index) => {
                            return <option value={year}>{year}</option>
                          })
            }
            </CSelect>

            <table>
                <tr>
                    <th style={{width:'5%',textAlign:'center'}}>No</th>
                    <th style={{width:'45%',paddingLeft:10}}>Nama File</th>
                    <th style={{width:'25%', textAlign:'center'}}>Tahun</th>
                    <th style={{width:'25%',textAlign:'center'}}>Tools</th>
                </tr>
                    {
                        data.map((res,index)=>{
                            return(
                            <tr>
                                <th style={{width:'5%',textAlign:'center'}}>{index+1}</th>
                                <th style={{width:'45%',paddingLeft:10}}>{res.user_profile}</th>
                                <th style={{width:'25%', textAlign:'center'}}>{res.tahun}</th>
                                <th style={{width:'25%', textAlign:'center'}}>

                                <CButton style={{marginBottom:10,marginTop:10}} onClick={()=>{window.open(""+window.server+"rest-api-sip/profile/skp/uploadskptahunan/file/"+res.user_profile+"")}}  color="info">Lihat Dokumen</CButton>
                                <CButton value={res.id} style={{marginBottom:10,marginTop:10,marginLeft:10}} onClick={handleDelete}  color="danger">Hapus</CButton>

                                </th>
                            </tr>
                            )
                        })
                    }
                </table>
          </CCol>

        </CRow>
      </CCard>
    </CContainer>
    )
}

export default SkpTahunanList
