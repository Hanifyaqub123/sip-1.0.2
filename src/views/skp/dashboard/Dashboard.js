import React, { useState, useEffect } from "react";
import {
  CDataTable,
  CBadge,
  CCollapse,
  CCardBody,
  CSpinner,
  CButton,
  CInput,
  CContainer,
  CRow,
  CCol,
  CCard,
  CSelect
} from "@coreui/react";
import {  useHistory } from 'react-router-dom'
import axios from 'axios'

const Dashboard = () => {
  const history = useHistory();

  // DATA TABLE DUMMY
  const usersData = [
    {id: 0, tahun: '2021', nilai: '80'}
  ]

  // const usersData1 = [
  //   {id: 0, nik: '3319050507970001', tanggal: '15-11-2021',uraian_tugas:'asasas',ket:'ok',status:'Pending'},
  //   {id: 0, nik: '33190505079700021', tanggal: '15-11-2021',uraian_tugas:'asasas',ket:'ok',status:'Active'}
  // ]

  const [details, setDetails] = useState([])

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  const fields = [
    { key: 'tahun', _style: { width: '20%'} },
    { key: 'nilai', _style: { width: '20%'} }
  ]

  const fields1 = [
    { key: 'nik', _style: { width: '10%'} },
    { key: 'tanggal', _style: { width: '10%'} },
    { key: 'uraian_tugas', _style: { width: '20%'} },
    { key: 'ket', _style: { width: '20%'} },
    { key: 'status', _style: { width: '5%'} },
    {
      key: 'show_details',
      label: '',
      _style: { width: '1%' },
      sorter: false,
      filter: false
    }
  ]

  const getBadge = (status)=>{
    switch (status) {
      case 'Active': return 'success'
      case 'Inactive': return 'secondary'
      case 'Pending': return 'warning'
      case 'Banned': return 'danger'
      default: return 'primary'
    }
  }

  // ------------------------------

  const [dataPejabat,setDataPejabat] = useState([])
  const [namaLengkap] = useState(localStorage.getItem('nama_lengkap'))
  const [id] = useState(localStorage.getItem('id'))
  const [pejabatPenilai,setPejabatPenilai] = useState("")
  const [nipPejabatPenilai,setNipPejabatPenilai] = useState("")
  const [userData1,setUserData1] = useState([]);
  const [realisasiHarian,setRealisasiHarian] = useState([]);

  const [nik] = useState(localStorage.getItem('nik'))
  const [image] = useState(localStorage.getItem('image'))
  const [namaJabatan, setNamaJabatan] = useState("")
  const [unitKerja, setUnitKerja] = useState("")
  const [valueDropdowonPejabat,setValueDropdownPejabat] = useState(true)
  const [namaPejabat, setNamaPejabat] = useState("")

  useEffect(() => {
    handleValidationLogin()
    handleValueRealisasiHarian()
    handleTableValue()
    handleUserPejabat()
    handleValidationPejabat()
  }, [])

  const handleValidationLogin = () =>{
    if (nik === null) {
      history.push('/login')
    } else {
      // alert(user_nik)
    }
  }

  const handleValueRealisasiHarian = () =>{
    axios.get(""+window.server+"rest-api-sip/profile/skp/realisasiharian/readskp.php?nik="+nik+"")
      .then((res)=>{
        if (res.data.message === "No post found") {
          setUserData1([])
        } else {
          setUserData1(res.data)
        } 
      })
  }
  
  
  const handleValidationPejabat = () =>{
    if (nik === null) {
      history.push('/login')
    } else {
      axios.get("" + window.server + "rest-api-sip/profile/login/validationpejabat.php?nik="+nik+"")
      .then((res) => {
        res.data.map((res,index)=>{
          if (res.pejabat_penilai === "") {
              setValueDropdownPejabat(true)
          } else {
              setValueDropdownPejabat(false)
              setPejabatPenilai(res.pejabat_penilai)
  
          }
        })
      })
    }
  

  }

  const handleUserPejabat = () => {
    axios.get("" + window.server + "rest-api-sip/profile/user/userpejabat/getuserpejabat.php")
      .then((res) => {
        setDataPejabat(res.data)
      })
  }

  const handleTableValue = () => {
    axios.get("" + window.server + "rest-api-sip/profile/user/riwayatjabatan/getjabatan.php?nik=" + nik + "")
      .then((res) => {
        // alert(JSON.stringify(res.data.pop()))
        if (res.data.message === "No post found") {
          setNamaJabatan("-")
          setUnitKerja("-")
        } else {
          var data = res.data.pop();
          setNamaJabatan(data.nama_jabatan)
          setUnitKerja(data.unit_kerja)
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  const handleSelectPejabat = e =>{

    var nip = e.target.value
    nip = nip.substring(nip.indexOf("-,") + 2);
    var nip_pejabat = nip
    setNipPejabatPenilai(nip_pejabat)
    var nama = e.target.value

    nama = nama.split(",-")[0]
    var nama_pejabat = nama
    setNamaPejabat(nama_pejabat)

  }

  const handleUpdatePejabat = () =>{
    var post ={
      id : id,
      pejabat_penilai : namaPejabat,
      nip_pejabat : nipPejabatPenilai
    }
    if (namaPejabat === "") {
        alert("Pilih nama pejabat terlebih dahulu")
    } else {
      axios.put(""+window.server+"rest-api-sip/profile/login/updatepejabatpenilai.php",post)
      .then((res)=>{
        alert(JSON.stringify(res.data))
        window.location.reload()
      })
    }
   
  }

  const handleDelete = (e) => {
    if (window.confirm('Apakah anda yakin ?')) {
      axios
      .delete("" + window.server + "rest-api-sip/profile/skp/realisasiharian/deleteskp.php", {
        data: { no: e.target.value },
      })
      .then((res) => {
      
      alert(JSON.stringify(res.data))
      window.location.reload();
      });
  }
   
  };


  return (
    <CContainer>
      <CCard>
        <CRow style={{ padding: 20, paddingBottom: 0 }}>
          <CCol lg="2" className="py-3">
            <img
              style={{ width: 150, height: 160, borderRadius: 3 }}
              src={image}
            />
          </CCol>
          <CCol style={{ flexDirection: "row" }} md="10" className="py-3">
            <div style={{ marginTop: 15 }}>
              <div>
                <label style={{ paddingRight: 10, fontSize: 15 }}>Nama : </label>
                <label style={{ fontSize: 15 }}>{namaLengkap}</label>
              </div>
              <div>
                <label style={{ paddingRight: 10, fontSize: 15 }}>NIK : </label>
                <label style={{ fontSize: 15 }}>{nik}</label>
              </div>
              <div>
                <label style={{ paddingRight: 10, fontSize: 15 }}>Jabatan : </label>
                <label style={{ fontSize: 15 }}>{namaJabatan}</label>
              </div>
              <div>
                <label style={{ paddingRight: 10, fontSize: 15 }}>Unit Kerja : </label>
                <label style={{ fontSize: 15 }}>{unitKerja}</label>
              </div>
              
               { 
               valueDropdowonPejabat === true &&
               <div style={{ display: 'flex', flexDirection: 'row' }}>
                   <CSelect onChange={handleSelectPejabat} style={{width:'40%',marginRight:20}} custom name="ccmonth" id="ccmonth">
                     <option value="">-- Pilih Pejabat Penilai --</option>
                      {
                        dataPejabat.map((res,index)=>{
                          return(
                            <option value={[res.nama_pejabat,"-",res.nip]}>{res.nama_pejabat}</option>
                          )
                        })
                      }
                    </CSelect>
                  <CButton onClick={handleUpdatePejabat} color="primary" className="px-4">Simpan</CButton>
                </div>}

                { 
               valueDropdowonPejabat === false &&
               <div style={{ display: 'flex', flexDirection: 'row' }}>
                   <CSelect  onChange={handleSelectPejabat} style={{width:'40%',marginRight:20}} custom name="ccmonth" id="ccmonth">
                     <option value={pejabatPenilai}>{pejabatPenilai}</option>
                      {
                        dataPejabat.map((res,index)=>{
                          return(
                            <option value={[res.nama_pejabat,"-",res.nip]}>{res.nama_pejabat}</option>
                          )
                        })
                      }
                    </CSelect>
                  <CButton onClick={handleUpdatePejabat} color="primary" className="px-4">Simpan</CButton>
                </div>}
              


            </div>


          </CCol>

        </CRow>

        <CRow style={{ padding: 20, }}>
          {/* <CCol lg="12" className=" py-3">
            <h3 style={{ paddingBottom: 10 }}>Nilai SKP</h3>
            <CDataTable
              items={usersData}
              fields={fields}
              columnFilter
              tableFilter
              footer
              itemsPerPageSelect
              itemsPerPage={5}
              hover
              sorter
              pagination
            />

          </CCol> */}

          <CCol lg="12" className=" py-3">
            <h3 style={{ paddingBottom: 10 }}>Realisasi Harian</h3>
            <CDataTable
              items={userData1}
              fields={fields1}
              columnFilter
              tableFilter
              footer
              itemsPerPageSelect
              itemsPerPage={5}
              hover
              sorter
              pagination
              scopedSlots = {{
                'status':
                  (item)=>(
                    <td>
                      <CBadge color={getBadge(item.status)}>
                        {item.status}
                      </CBadge>
                    </td>
                  ),
                'show_details':
                  (item, index)=>{
                    return (
                      <td className="py-2">
                        <CButton
                          color="primary"
                          variant="outline"
                          shape="square"
                          size="sm"
                          onClick={()=>{toggleDetails(index)}}
                        >
                          {details.includes(index) ? 'Hide' : 'Show'}
                        </CButton>
                      </td>
                      )
                  },
                'details':
                    (item, index)=>{
                      return (
                      <CCollapse show={details.includes(index)}>
                        <CCardBody>
                          <h4>
                            {item.username}
                          </h4>
                          <p className="text-muted">NIK: {item.nik}</p>
                          <CButton onClick={()=>{
                              history.push("/editskpharian");
                              localStorage.setItem("data", JSON.stringify(item));
                          }} size="sm" color="info">
                            Edit SKP
                          </CButton>
                          <CButton
                           value={item.no}
                           onClick={handleDelete}
                           size="sm" color="danger" className="ml-1">
                            Delete
                          </CButton>
                        </CCardBody>
                      </CCollapse>
                    )
                  }
              }}
            />

          </CCol>

        </CRow>
      </CCard>
    </CContainer>
  )
}

export default Dashboard