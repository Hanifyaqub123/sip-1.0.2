import React, { useState, useEffect,useCallback } from "react";
import {
  CButton,
  CContainer,
  CRow,
  CCol,
  CCard,
  CButtonToolbar,
  CInput,
  CSelect
} from "@coreui/react";
import { useHistory } from "react-router-dom";
import axios from "axios";


const BuatSkp = () =>{
  const year = (new Date()).getFullYear();
  const years = Array.from(new Array(10),( val, index) => index + year);

  const history = useHistory();
  const [nik] = useState(localStorage.getItem('nik'))
  const [buttonValue,setButtonValue] = useState(false)

  const [pejabatPenilai,setPejabatPenilai] = useState('')
  const [nipPejabatPenilai,setNipPejabatPenilai] = useState('')
  const [jabatanPenilai,setJabatanPenilai] = useState('')
  const [unitKerjaPenilai,setUnitKerjaPenilai] = useState('')

  const [kuantitas,setKuantitas] = useState([])
  const [totalKuantitas,setTotalKuantitas] = useState('')
  const [kualitas,setKualitas] = useState([])
  const [waktu,setWaktu] = useState([])
  const [tahun,setTahun] = useState('')
  const [nipPenilai,setNipPenilai] = useState('')

  const[namaPPNPN,setNamaPPNPN] = useState('')
  const [jabatanPPNPN,setJabatanPPNPN] = useState('')
  const [unitKerjaPPNPN,setUnitKerjaPPNPN] = useState('')

  const [data,setData] = useState([])

  useEffect(()=>{
    handleGetProfile()
    handleGetJabatanUser()
    axios.get(""+window.server+"rest-api-sip/profile/skp/sasaranskp/getsasaranskp.php?nik="+nik+"")
      .then((res)=>{
        
        if (res.data.message === "No post found" ) {
            setData([])
            setButtonValue(true)
        } else {
          var array = JSON.parse(res.data[0].kegiatan_tugas.replace(/&quot;/g,'"'))
          setData(array)
          setButtonValue(false)
        }

      })
  },[])

  const handleGetProfile = () =>{
    axios.get(""+window.server+"rest-api-sip/profile/user/identitas/getprofile.php?nik="+nik+"")
      .then((res)=>{
        res.data.map((res)=>{
          setNamaPPNPN(res.nama_lengkap)
          
          axios.get(""+window.server+"rest-api-sip/profile/user/userpejabat/getuserpejabatfilter.php?nip="+res.nip_pejabat+"")
          .then((res)=>{
            if (res.data.message === "No post found") {
              setNipPejabatPenilai('-')
              setJabatanPenilai('-')
              setUnitKerjaPenilai('-')
              setPejabatPenilai('-')
              alert('Mohon isi pejabat penilai terlebih dahulu di Dashboard menu SKP !')
            } else {
              res.data.map((res)=>{
                setPejabatPenilai(res.nama_pejabat)
                setNipPejabatPenilai(res.nip)
                setJabatanPenilai(res.jabatan)
                setUnitKerjaPenilai(res.unit_kerja)
              })
            }
             
          })
        })
      })
  }

  const handleGetJabatanUser = () =>{
    axios.get(""+window.server+"rest-api-sip/profile/user/riwayatjabatan/getjabatan.php?nik="+nik+"")
    .then((res)=>{
        if (res.data.message === 'No post found') {
          setJabatanPPNPN('-')
          setUnitKerjaPPNPN('-')
        } else {
          res.data.map((res)=>{
            setJabatanPPNPN(res.nama_jabatan)
            setUnitKerjaPPNPN(res.unit_kerja)
          })
        }
    })
  }



  const handleKirimAtasan = () =>{
  
    const post = {
      nik : nik,
      kegiatan_tugas : JSON.stringify(data)
      // kuantitas : kuantitas,
      // kualitas : kualitas,
      // waktu : waktu,
      // tahun : "2021",
      // status : 'Pending',
      // nip_penilai : '11221221313'
    }
    // var sumKuantitas = 0;
    // for (let i = 0; i< kuantitas.length; i++) {
    //     const element = kuantitas[i]
    //     const JualBeliTotal = Number(element)
    //     sumKuantitas = sumKuantitas + JualBeliTotal;

    // }
    // setTotalKuantitas(sumKuantitas)

    alert(JSON.stringify(post))
  }

  

  
// const handleRemoveList = e=>{


//   if (window.confirm('Apakah anda yakin untuk menghapus ?')) {
//     axios.delete(""+window.server+"rest-api-sip/profile/skp/buatskp/deletedaftarskp.php",{
//         data: { id: e.target.value },
//       })
//         .then((res)=>{
//             alert(JSON.stringify(res.data))
//             window.location.reload();
//         })
//         .catch((err)=>{
//             console.log(err)
//         })
//   }


//   var array = [...data]
//   const index = e.target.value
//   if (index > -1) {
//     array.splice(index, 1);
//     setData(array)
//   }

// }

    return(
        <CContainer>
        <CCard style={{padding:20}}> 
          <h3>Data Pegawai</h3>
        <CRow>
          <CCol lg="6" className="py-3">
            <div>
            <label>Pejabat Penilai : &nbsp;</label>
            </div>
            <div>
            <label>Nama : &nbsp;</label>
            <label>{pejabatPenilai}</label>
            </div>
            <div>
            <label>NIP : &nbsp;</label>
            <label>{nipPejabatPenilai}</label>
            </div>
            <div>
            <label>Jabatan : &nbsp;</label>
            <label>{jabatanPenilai}</label>
            </div>
            <div>
            <label>Unit Kerja : &nbsp;</label>
            <label>{unitKerjaPenilai}</label>
            </div>
          </CCol>
          <CCol md="6" className="py-3">
          <div>
            <label>Pegawai yang dinilai : &nbsp;</label>
            </div>
            <div>
            <label>Nama : &nbsp;</label>
            <label>{namaPPNPN}</label>
            </div>
            <div>
            <label>NIK : &nbsp;</label>
            <label>{nik}</label>
            </div>
            <div>
            <label>Jabatan : &nbsp;</label>
            <label>{jabatanPPNPN}</label>
            </div>
            <div>
            <label>Unit Kerja : &nbsp;</label>
            <label>{unitKerjaPPNPN}</label>
            </div>
          </CCol>
        </CRow>
        </CCard>
        <CCard style={{padding:20}}> 
          <h3>Sasaran SKP</h3>
          <CButtonToolbar style={{paddingTop:20,paddingBottom:20}} justify="end">
        <CButton onClick={()=>history.push("/pilihskp")} style={{marginRight:10}} color="danger">Buat SKP</CButton>
      

    </CButtonToolbar>
    <CCol xs="12" md="3">

      
      
                    <CSelect 
                      name="disabledSelect" 
                      id="disabledSelect" 
                      autoComplete="name"
                    >
                       <option value="0">--- Pilih Tahun ---</option>
                       {
                          years.map((year, index) => {
                            return <option value={year}>{year}</option>
                          })
                        }
                     
                      
                    
                    </CSelect>
          </CCol>
          <CCol  className="py-3">
        
<table>
  <tr>
    <th style={{width:'3%',textAlign:'center'}} rowSpan="2">No</th>
    <th style={{width:'40%',textAlign:'center'}}rowSpan="2">Kegiatan Tugas</th>
    <th style={{width:'20%',textAlign:'center'}} colSpan="3">Target</th>

  </tr>
  <tr>
    <th style={{width:'5%',backgroundColor:'#ffffff',textAlign:'center'}}>Kuantitas / Output</th>
    <th style={{width:'5%',backgroundColor:'#ffffff',textAlign:'center'}}>Kualitas / Mutu</th>
    <th style={{width:'10%',backgroundColor:'#ffffff',textAlign:'center'}}>Waktu (Bulan)</th>
  </tr>
  {
      data.map((res,index)=>{
        return(
          <tr>
            <th style={{textAlign:'center'}}>{index+1}</th>
            <th>
            <CInput
                        value={res.nama_kegiatan}
                        type="text"
                        
            />
            
            </th>
            <th>
            <CInput
                        key={index+1}
                        name={index+1}
                        value={kuantitas.index}
                        onChange={(e)=>{
                          let values = [...kuantitas]
                          values[index] = e.target.value
                          setKuantitas(values)
                        }}
                        style={{textAlign:'center'}}
                        type="number"
                        placeholder="0"
                        
            />
            </th>
            <th>
            <CInput
                        key={index+1}
                        name={index+1}
                        value={kualitas.index}
                        onChange={(e)=>{
                          let values = [...kualitas]
                          values[index] = e.target.value
                          setKualitas(values)
                        }}
                        style={{textAlign:'center'}}
                        type="number"
                        placeholder="0"
            />
            </th>
            <th style={{display:'flex', flexDirection:'row'}}>
            <CInput
                        key={index+1}
                        name={index+1}
                        value={waktu.index}
                        onChange={(e)=>{
                          let values = [...waktu]
                          values[index] = e.target.value
                          setWaktu(values)
                        }}
                        style={{textAlign:'center'}}
                        type="number"
                        placeholder="0"
            />
            </th>
         
          </tr>
        )
      })
  }

{/* <tr>
    <th style={{width:'5%',backgroundColor:'#ffffff',textAlign:'center'}}></th>
    <th style={{width:'5%',backgroundColor:'#ffffff',textAlign:'center'}}>Jumlah</th>
    <th style={{width:'10%',backgroundColor:'#ffffff',textAlign:'center'}}>{totalKuantitas}</th>
    <th style={{width:'10%',backgroundColor:'#ffffff',textAlign:'center'}}></th>
    <th style={{width:'10%',backgroundColor:'#ffffff',textAlign:'center'}}></th>
  </tr> */}
  

 
</table>

        
        </CCol>
        <CButtonToolbar style={{paddingTop:20,paddingBottom:20}} justify="end">
        {
          buttonValue === false &&
          <CButton onClick={handleKirimAtasan} style={{marginRight:10}} color="success">Kirim Atasan</CButton>
        }
        <CButton style={{marginRight:10}} color="info">Cetak</CButton>

    </CButtonToolbar>
        </CCard>
      </CContainer>
    )
}

export default BuatSkp