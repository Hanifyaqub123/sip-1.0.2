import React,{useState} from 'react'
import {  useHistory } from 'react-router-dom'
import {
    CForm,
    CCol,
    CContainer,
    CRow,
    CLabel,
    CInput,
    CFormGroup,
    CTextarea,
    CButtonToolbar,
    CButton,
    CSpinner
  } from '@coreui/react'

import axios from 'axios';


const EditSkpHarian = () => {
    // useEffect(()=>{
    //     const dataEdit = JSON.parse(reactLocalStorage.get("data"))
    //     alert(JSON.stringify(dataEdit.tangg))

    // })
  const history = useHistory();
  const [loading,setLoading] = useState(true)
  const user = JSON.parse(localStorage.getItem("data"))
  const user1 = JSON.parse(localStorage.getItem("user"))
  const [nik] = useState(user.nik);
  const [namaLengkap] = useState(localStorage.getItem('nama_lengkap'));
  const [ket,setKet] = useState(user.ket);
  const [tanggal,setTanggal] = useState(user.tanggal);
  const [uraianTugas,setUraianTugas] = useState(user.uraian_tugas);

  const handleKeterangan = e =>{
    setKet(e.target.value)
  }

  const handleUraianTugas = e =>{
    setUraianTugas(e.target.value)
  }

  const handleDate = e =>{
    setTanggal(e.target.value)
  }
  
  const handleAdd = e =>{
    const post = {
        "no":user.no,
        "tanggal" : tanggal,
        "uraian_tugas" : uraianTugas,
        "ket" : ket,
        "nik" : nik,
        "status": "Pending",
        "nama_lengkap" : namaLengkap,

    }

    if(tanggal === ""){
      alert("Tanggal tidak boleh kosong")
    }else if(ket === ""){
      alert("Keterangan tidak boleh kosong")

    }else if(uraianTugas === ""){
      alert("Uraian tugas tidak boleh kosong")
    }else{
      setLoading(false)
      if (window.confirm('Apakah anda yakin ?')) {
        axios.put(""+window.server+"rest-api-sip/profile/skp/realisasiharian/editskp.php",post)
        .then((res)=>{
          setLoading(true)
          alert(JSON.stringify(res.data))
          localStorage.removeItem("data");
          history.push('/skp')        
        })
    }
      
    
    }
  }


  return (
    <CContainer>
    <CRow>
      <CCol sm="6">
        <CForm action="" method="post">
          <CFormGroup>
            <CLabel htmlFor="nf-email">NIK</CLabel>
            <CInput
              value={nik}
              type="nik"
              id="nik"
              name="NIK"
              placeholder="NIK"
              autoComplete="NIK"
              size
            />
          </CFormGroup>
          <CFormGroup>
            <CLabel >Keterangan</CLabel>
            <CTextarea
             rows="10"
             onChange={handleKeterangan}
             value={ket}
             type="ket"
             id="ket"
             name="ket"
            />
          </CFormGroup>
          
        </CForm>
      </CCol>
      <CCol sm="6">
        <CForm action="" method="post">
        <CFormGroup>
            <CLabel >Tanggal</CLabel>
            <CInput
              value={tanggal}
              type="date"
              id="date"
              name="date"
              placeholder="date"
              autoComplete="date"
              onChange={handleDate}
            />
          </CFormGroup>

          <CFormGroup>
            <CLabel >Uraian Tugas</CLabel>
            <CTextarea
             rows="10"
             value={uraianTugas}
             type="uraianTugas"
             id="uraianTugas"
             name="uraianTugas"
             onChange={handleUraianTugas}
            />
          </CFormGroup>
        
        </CForm>
      </CCol>
      
   
    </CRow>
    { loading === false &&
       <div style={{position:'absolute', bottom:"25%"}}>
       <CSpinner
               
               color="primary"
               style={{width:'4rem', height:'4rem'}}
         />
       </div>
    }
   
      <CButtonToolbar  style={{marginBottom:'3%', marginTop:'3%',marginRight:'3%'}} justify="end">
      <CButton onClick={handleAdd} color="danger">Edit</CButton>
      </CButtonToolbar>
      
  </CContainer>
    )
}

export default EditSkpHarian
