import React,{useState} from 'react'
import {
    CButtonToolbar,
    CForm,
    CCol,
    CContainer,
    CSelect,
    CInput,
    CButton,
    CCard
  } from '@coreui/react'
  import axios from 'axios'
  import {  useHistory } from 'react-router-dom'


const UploadSkpTahunan = () =>{
    const [nik] = useState(localStorage.getItem('nik'))
    const history = useHistory();

    const year = (new Date()).getFullYear();
    const years = Array.from(new Array(10),( val, index) => index + year);
    const [ButtonUploadView,setButtonView] = useState(false)
    const [uploadProgressView,setUploadProgressView] = useState()
    const [percentageUpload,setPercentageUpload] = useState(0)
    const [pdfView,setPdfview] = useState(false)

    const [imageSelected,setImageSelected] = useState("")
    const [urlImage,setUrlImage] = useState("")
    const [tahun,setTahun] = useState("")


    const uploadImage = () =>{
        if (tahun === "") {
            alert("Tahun tidak boleh kosong")
        } else if (imageSelected === "") {
            alert("Gambar tidak boleh kosong")
        } else {
            setButtonView(true)
            setUploadProgressView(true)
    
                const options =  {
                    onUploadProgress : (progressEvent) =>{
                        const {loaded,total} = progressEvent;
                        let percent = Math.floor(loaded * 100 / total) 
                        // console.log(` ${loaded}kb of ${total}kb | ${percent}% `);
                        setPercentageUpload(percent)
                        if (percent === 100) {
                            setUploadProgressView(false)
                            setPdfview(true)
                        }
                    }
                }
        
        
                const formData  = new FormData()
                  formData.append("user_image",imageSelected)
                  formData.append("user_name",nik)
                  formData.append("tahun",tahun)
    
                  axios.post(""+window.server+"rest-api-sip/profile/skp/uploadskptahunan/upload.php",formData,options)
                  .then((res)=>{
                    alert(JSON.stringify(res.data.MESSAGE))
                    history.push('/listskptahunan')
                    // setUrlImage(res.data.url_image)
                  })
        }
      
        
      }

      const handleTahun = e =>{
          setTahun(e.target.value)
      }


    return(
        <CContainer>
            <CCard style={{padding:20}}>
            <CSelect onChange={handleTahun} style={{width:'20%',marginRight:20,marginBottom:20}} custom name="ccmonth" id="ccmonth">
            <option value="">-- Pilih Tahun SKP --</option>
            {
                          years.map((year, index) => {
                            return <option value={year}>{year}</option>
                          })
            }
            </CSelect>
            <h5>Upload Berkas SKP Tahunan</h5>
            <div>Penamaan Format : Fajar Eka_SKP_2021.pdf</div>
            <div style={{paddingBottom:10}}>
            <input 
            style={{paddingTop:10,paddingRight:10}}
                        type="file"
                        onChange={(e)=>{
                            const { target } = e
                            if(target.value.length > 0){
                            var filename = e.target.files[0].name
                            var fileExtension = filename.split('.').pop();
                            if (fileExtension === "pdf") {
                                setImageSelected(e.target.files[0]);
                            }else{
                                alert("File bukan PDF")
                                setImageSelected("");
                            }
                            } else {
                                alert("File tidak boleh kosong, pilih file kembali")
                                setImageSelected("");
                            }
                            
                        }}
                        />
                         <button 
                        style={{marginRight:'5%'}}
                        onClick={uploadImage}
                        >Upload PDF</button>
            </div>
            {/* {
                        pdfView === true &&

                        <a target="_blank" href={""+window.server+"rest-api-sip/profile/skp/uploadskptahunan/file/"+urlImage}>Cek Data</a>

            } */}
                        
            </CCard>
        </CContainer>
    )
}

export default UploadSkpTahunan;